import pg from "pg";
import "dotenv/config.js";
import { v4 as uuidv4 } from 'uuid';
import queries from "./queries.js";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: String(PG_PASSWORD),
    database: PG_DATABASE,
});

const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    } finally {
        client.release();
    }
};

/* POST */
export const createProductsTable = async () => {
    const query = `
        CREATE TABLE IF NOT EXISTS products (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL,
            price REAL NOT NULL,
            productCode VARCHAR(255) NOT NULL
        )`;
    await executeQuery(query);
    console.log("Products table initialized");
};
export const insertProduct = async (body) => {
    const id = uuidv4();
    const params = [...Object.values(body), id];
    console.log(`Inserting a new ${params[0]}`);
    const result = await executeQuery(queries.insertProduct, params);
    console.log(`New Product ${params[0]} inserted successfully.`);

    return result;
}

/* GET */
export const getProducts = async (body) => {

    const result = await executeQuery(queries.getProducts);
    console.log(`Products ${JSON.stringify(result.rows)}`);
    return result;
}
export const getProduct = async (id) => {

    const params = [id];
    const result = await executeQuery(queries.getProduct, params);
    console.log(`Products ${JSON.stringify(result.rows)}`);
    return result;
}

/* PUT */
export const updateProduct = async (body) => {

    const params = [...Object.values(body)];
    console.log(`Updating product ${params[0]}`);
    const result = await executeQuery(queries.updateProduct, params);
    console.log(`Product ${params[0]} update successfully.`);

    return result;
}

/* DELETE */
export const deleteProduct = async (body) => {

    const params = [...Object.values(body)];
    console.log(`Removing product ${params[0]}`);
    const result = await executeQuery(queries.deleteProduct, params);
    console.log(`Product ${params[0]} removed successfully.`);
    return result;
}

