import express from "express";
import "dotenv/config.js";
import {
  createProductsTable,
  deleteProduct,
  getProducts,
  getProduct,
  insertProduct,
  updateProduct
} from "./db.js";

const server = express();
server.use(express.json());

const { PORT } = process.env;

/* POST */
server.post("/table/create", (req, res) => {
  createProductsTable().then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})
server.post("/product/insert", (req, res) => {
  insertProduct(req.body).then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})


/* GET */
server.get("/table/products", (req, res) => {
  getProducts(req.body).then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})
server.get("/table/product/:id", (req, res) => {
  getProduct(req.params.id).then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})
server.get("/server", (req, res) => {
  console.log(process.env.PORT);
  console.log(process.env.PG_HOST);
  console.log(process.env.PG_USERNAME);
  console.log(process.env.PG_PASSWORD);
  console.log(process.env.PG_DATABASE);
  res.sendStatus(200);
})


/* PUT */
server.put("/product/update/", (req, res) => {
  updateProduct(req.body).then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})


/* DELETE */
server.delete("/product/delete", (req, res) => {
  deleteProduct(req.body).then(() =>
    res.sendStatus(200)
  ).catch(() =>
    res.sendStatus(400)
  );
})



server.listen(PORT, () => {
  console.log("Products API listening to port", PORT);
});
