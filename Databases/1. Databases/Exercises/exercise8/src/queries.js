
const queries = {
    insertProduct:
        'INSERT INTO products(name, price, productcode) VALUES($1, $2, $3)',
    updateProduct:
        'UPDATE products SET name = $2, price = $3, productcode = $4 WHERE id = $1',
    deleteProduct:
        'DELETE FROM products WHERE productcode=$1',
    getProducts:
        'SELECT * FROM products',
    getProduct:
        'SELECT * FROM products WHERE id = $1',
}

export default queries