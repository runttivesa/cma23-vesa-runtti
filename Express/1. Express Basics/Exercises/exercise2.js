import express from 'express';
import url from 'url';
const server = express();

// Parse JSON bodies for this app. Make sure you put
// `app.use(express.json())` **before** your route handlers!
server.use(express.json());

let dogsArr = [];

const data = [
    { id: 1, name: 'John' },
    { id: 2, name: 'Jane' }
]

server.post('/endpoint2', function (req, res) {
    var dog = req.body;
    console.log(dog);
    dogsArr.push(dog);
    res.send("Dog added!");
});


server.get("/getdogs", (req, res) => {
    console.log('Sanity: ' + JSON.stringify(dogsArr));
    res.end(JSON.stringify(dogsArr));
});


server.get("/getuser/:name/:surname", (request, response) => {
    console.log("Params:");
    console.log(request.params);
    console.log("Query:");
    console.log(request.query);

    response.send(`Hello ${request.params.name} ${request.params.surname}`);
});


server.get('/getid/:id', (req, res) => {
    const id = Number(req.params.id)
    const info = data.find(item => item.id === id)
    if (info === undefined) {
        return res.status(404).send()
    }
    res.send(info)
})

server.get('/', (req, res) => {
    res.send("Hello World");
})

// URL module usage example
/* const adr = 'http://localhost:5000/default.html?year=2017&month=february'; // Define the address
const q = url.parse(adr, true); // Parse through the address with url.parse() function.

console.log(q.host); // returns 'localhost:5000'
console.log(q.pathname); // returns '/default.html'
console.log(q.search); // returns '?year=2017&month=february'

const qdata = q.query; // returns an object: { year: 2017, month: 'february' }
console.log(qdata.month); // returns 'february */


const app = await server.listen(3000);