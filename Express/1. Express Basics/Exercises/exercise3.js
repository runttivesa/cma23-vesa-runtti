import express from 'express';
const server = express();

server.use(express.json());

let counter = 0;

server.get('/counter', (req, res) => {
  counter += 1;
  res.send(`counter = ${counter}`);
});

const app = await server.listen(3000);