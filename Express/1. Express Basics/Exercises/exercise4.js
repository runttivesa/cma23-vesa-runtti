import express from 'express';
const server = express();

server.use(express.json());

let users = {};

server.get('/counter/:name', (req, res) => {

  if (Object.keys(users).length == 0)
    users[req.params.name] = 1;
  else {
    if (req.params.name in users)
      users[req.params.name] += 1;
    else
      users[req.params.name] = 1;
  }

  res.send(`${JSON.stringify(users)}`);
});

const app = await server.listen(3000);