import express from 'express';
const server = express();
import { logger, students } from './middleware5.js';

server.use(express.json());


server.get('/students', logger(), (req, res) => {
  res.send(`requestInfo = ${req.requestInfo} \n students = ${students}`);
});

const app = await server.listen(3000);