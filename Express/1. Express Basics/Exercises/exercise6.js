import express from 'express';
import paths from './paths.js';
import { logger, checkEndpoint, checkPath } from './middleware6.js';

const server = express();

server.use(express.json());


server.use(checkPath)

server.get("/get", logger(), async (req, res) => {

  try {
    return res.send(`requestInfo = ${req.requestInfo}`);
  }
  catch (error) {
    throw error;
  }
});

server.use(checkEndpoint)

const app = await server.listen(3000);