import express from 'express';
import bodyParser from 'body-parser';

const server = express();

server.use(bodyParser.json());

const logger = () => {
    return (req, res, next) => {
        let currentdate = new Date();
        req.requestInfo = "Last request: " + currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear() + " @ "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds() + " / "
            + req.method + " / "
            + `http://localhost:3000${req.path}`;

        console.log(`requestInfo = ${req.requestInfo}`)

        next();
    }
}

server.post('/', logger(), (req, res) => {
    console.log(req.body);
    res.send(`requestInfo = ${req.requestInfo} \n ${req.body ? JSON.stringify(req.body) : ""}`);
});


const app = await server.listen(3000);