import express from 'express';
const server = express();
import { Student } from './studentSchema.js';

server.use(express.json());

server.post('/student', (req, res) => {

  if (
    !req.body.id || !req.body.name || !req.body.email
  ) {
    res.sendStatus(400);
  }

  const newStudent = new Student(
    req.body.id,
    req.body.name,
    req.body.email,
  )

  console.log(newStudent);

  res.sendStatus(201);
});

server.get('/student/:id', (req, res) => {

  let student = Student.all.find(element => element.id === req.params.id)
  console.log(student);
  if (student)
    res.send(student);
  else
    res.sendStatus(404);
});

const app = await server.listen(3000);