import express from 'express';
const server = express();
import { Student } from './studentSchema.js';

server.use(express.json());

/* POST */
server.post('/student', (req, res) => {

  if (
    !req.body.id || !req.body.name || !req.body.email
  ) {
    res.sendStatus(400);
  }

  let newStudent = new Student(
    req.body.id,
    req.body.name,
    req.body.email,
  )

  console.log(newStudent);
  res.sendStatus(201);
});

/* GET */
server.get('/student/:id', (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else 
    res.sendStatus(404);

  if (student)
    res.send(student);
  else if (student == undefined)
    res.sendStatus(404);
});

/* PUT */
server.put('/student/:id', (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else 
    res.sendStatus(404);

  if (student) {

    let newStudent = {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email,
    }
    let index = Student.all.indexOf(student);

    Student.all[index] = newStudent;

    console.log(Student.all);
    res.sendStatus(200)
  }
  else if (student == undefined) {
    res.sendStatus(400);
  }
});

/* DELETE */
server.delete('/student/:id', (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else 
    res.sendStatus(404);

  if (student) {
    let index = Student.all.indexOf(student);

    Student.all.splice(index, 1);

    res.sendStatus(204);
  }
  else if (student == undefined) {
    res.sendStatus(404);
  }
});


const app = await server.listen(3000);