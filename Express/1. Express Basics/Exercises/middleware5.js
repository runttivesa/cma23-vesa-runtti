
export const students = []

export const logger = () => {
    return (req, res, next) => {
  
      let currentdate = new Date();
      req.requestInfo = "Last request: " + currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + " @ "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds() + " / "
        + req.method + " / "
        + `http://localhost:3000${req.path}`;
  
      next();
    }
  }