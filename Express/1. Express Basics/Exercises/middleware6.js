
const paths = [
  "/get",
]

export const logger = () => {
  return (req, res, next) => {

    let currentdate = new Date();
    req.requestInfo = "Last request: " + currentdate.getDate() + "/"
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getFullYear() + " @ "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes() + ":"
      + currentdate.getSeconds() + " / "
      + req.method + " / "
      + `http://localhost:3000${req.path}`;

    next();
  }
}

/* Should be done at client side preferably
/* instead of using back end for processing */
export const checkPath = (req, res, next) => {
  if (res.statusCode == 200) {
    if (paths.find((path) => path !== req.path))
      res.status(404).send("404 error, no path found")
    next();
  }
}

export const checkEndpoint = (req, res, next) => {

  /* Check if response headders already sent */
  if (!res.headersSent)
    res.status(404).send("404 error, url not found")

  return;
}