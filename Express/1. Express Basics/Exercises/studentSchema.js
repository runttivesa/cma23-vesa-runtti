export class Student {

    static all = [];

    constructor(id, name, email) {
        this.id = id;
        this.name = name;
        this.email = email;
        Student.all.push(this);
    }
}