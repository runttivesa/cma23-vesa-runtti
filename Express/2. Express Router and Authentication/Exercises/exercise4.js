import express from 'express';
import dotenv from "dotenv";
import path from 'path';
import { fileURLToPath } from 'url';
import bodyParser from 'body-parser';
import studentRouter from "./routes/studentRouter.js";
import userRouter from "./routes/userRouter.js";

/* Env Config */
const env = dotenv.config()


/* Express */
const server = express();
const port = process.env.PORT || 3000;


/* Paths */
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


/* Parser */
server.use(bodyParser.urlencoded({ extended: true }))


/* Static info page */
server.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, '/exercise1.html'));
});


/* ---- End Points ---- */
server.use("/student", studentRouter);
server.use("/user", userRouter);


const app = await server.listen(3000);