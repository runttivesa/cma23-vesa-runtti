import jwt from 'jsonwebtoken';
import argon2 from "argon2";
import "dotenv/config.js";

export const jwtGenerate = (username) => {
  const payload = { username: username };
  const secret = process.env.SECRET;
  const options = { expiresIn: '15min' };

  return jwt.sign(payload, secret, options);
}

export const jwtVerify = (token) => {
  try {
    let decoded = jwt.verify(token, process.env.SECRET);
    if (decoded)
      return decoded;
    else
      return null;
  } catch (err) {
    // err
  }
}

export const passwordVerify = async (hashPassword, password) => {
  try {
    if (await argon2.verify(hashPassword, password)) {
      return true
    } else {
      return false
    }
  } catch (error) {
    throw (error)
  }
}