import express from 'express';
import "dotenv/config.js";
const router = express.Router();
router.use(express.json());
import { jwtGenerate, jwtVerify } from '../helpers/helpers.js';

/* ---- Routes ---- */
// Generate
router.post("/generate", (req, res) => {

  const token = jwtGenerate(req.body.username);

  if (token) {
    res.status(200).send(`${token}`);
  }
  else {
    res.sendStatus(400);
  }

  console.log(token);
});
// Verify
router.post("/verify", (req, res) => {

  const result = jwtVerify(req.body.token)
  if (result) {
    console.log(result);
    res.sendStatus(200);
  }
  else
    res.sendStatus(400);
});

export default router;