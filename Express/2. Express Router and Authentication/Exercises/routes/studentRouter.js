import express from 'express';
import { Student } from "../schemas/studentSchema.js"
import { jwtVerify } from '../helpers/helpers.js';
const router = express.Router();
router.use(express.json());

const authenticate = (req, res, next) => {
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Im1vbmtlaCIsImlhdCI6MTUxNjIzOTAyMn0.jwBEVnI1egOoyEZXV1dIBylw8ja6P5AzyZOxSha1zyA";
  headers.append("Authorization", `Bearer ${token}`);
  const auth = headers.get('Authorization').substring(7)
  try {
    if (jwtVerify(auth))
      next()

    return;
  } catch (error) {
    return res.status(401).send('Invalid token')
  }
}

/* ---- Routes ---- */
// Get
router.get("/:id", authenticate, (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else
    res.sendStatus(404);

  if (student) {
    res.send(student);
  }
  else if (student == undefined)
    res.sendStatus(404);
});
// Post
router.post("/", authenticate, (req, res) => {

  if (
    !req.body.id || !req.body.name || !req.body.email
  ) {
    res.sendStatus(400);
  }

  let newStudent = new Student(
    req.body.id,
    req.body.name,
    req.body.email,
  )

  console.log(newStudent);
  res.sendStatus(201);
});
// Update
router.put("/:id", authenticate, (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else
    res.sendStatus(404);

  if (student) {

    let newStudent = {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email,
    }
    let index = Student.all.indexOf(student);

    Student.all[index] = newStudent;

    console.log(Student.all);
    res.sendStatus(200)
  }
  else if (student == undefined) {
    res.sendStatus(400);
  }
});
// Delete
router.delete("/:id", authenticate, (req, res) => {

  let student;

  if (req.params?.id)
    student = Student.all.find(element => element.id === req.params.id)
  else
    res.sendStatus(404);

  if (student) {
    let index = Student.all.indexOf(student);

    Student.all.splice(index, 1);

    res.sendStatus(204);
  }
  else if (student == undefined) {
    res.sendStatus(404);
  }
});


/* ---- Export ---- */
export default router;