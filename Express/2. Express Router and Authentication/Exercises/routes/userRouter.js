import express from 'express';
import argon2 from "argon2";
import "dotenv/config.js";
const router = express.Router();
router.use(express.json());
import { User } from '../schemas/userSchema.js';
import { jwtGenerate, passwordVerify } from '../helpers/helpers.js';

/* ---- Routes ---- */
// User Register
router.post("/register", async (req, res) => {

  if (req.body.username && req.body.password) {

    /* Compare password */
    const hashedPassword = await argon2.hash(req.body.password).then(result => result);

    /* Create new user */
    const newUser = new User(
      req.body.username,
      hashedPassword
    )

    if (newUser) {

      /* Create token */
      const token = jwtGenerate(req.body.username);

      if (token) {

        console.log(`user: ${JSON.stringify(newUser).replace(/\\/g, "")}  /  token ${token}`);
        res.status(200).send(`user: ${JSON.stringify(newUser)}  /  token ${token}`);
      }
    }
    else {
      res.sendStatus(400)
    }
  }
  else {
    res.sendStatus(404)
  }

});
// User Login
router.post("/login", async (req, res) => {

  if (req.body.username && req.body.password) {

    /* Find User */
    const user = User.all.find(element => element.username === req.body.username);

    if (user) {

      /* Decrypt user password */
      const decryptPassword = passwordVerify(user.password, req.body.password);

      if (decryptPassword) {

        /* Generate access token */
        const token = jwtGenerate(req.body.username);

        if (token) {
          console.log(`user: ${JSON.stringify(user).replace(/\\/g, "")}  /  token ${token}`);
          res.status(200).send(`user: ${JSON.stringify(user)}  /  token ${token}`);
        }
      }
      else {
        res.sendStatus(401);
      }
    }
    else {
      res.sendStatus(404);
    }

  }
  else {
    res.sendStatus(400);
  }

});
// Admin Login
router.post("/admin", async (req, res) => {

  /* Verify admin user name and password */
  if (req.body.username && req.body.password) {
    if (req.body.username === process.env.ADMIN &&
      req.body.password === process.env.ADMINPW) {

      /* Generate access token */
      const token = jwtGenerate(req.body.username);

      if (token) {
        res.sendStatus(200);
      }
    }
    else
      res.sendStatus(401);
  }
  else {
    res.sendStatus(400);
  }

});

export default router