export class Student {

    static all = [
        {
            id: "1",
            name: "jack",
            email: "email@gmail.com"
        },
        {
            id: "2",
            name: "steve",
            email: "email@gmail.com"
        },
        {
            id: "3",
            name: "robert",
            email: "email@gmail.com"
        }
    ];

    constructor(id, name, email) {
        this.id = id;
        this.name = name;
        this.email = email;
        Student.all.push(this);
    }
}