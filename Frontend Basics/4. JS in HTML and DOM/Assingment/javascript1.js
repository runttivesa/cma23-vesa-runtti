const students = [
  { name: "Sami", score: 24.75 },
  { name: "Heidi", score: 20.25 },
  { name: "Jyrki", score: 27.5 },
  { name: "Helinä", score: 26.0 },
  { name: "Maria", score: 17.0 },
  { name: "Yrjö", score: 14.5 }
];

function getUsers() {

  if (document.getElementById("studentTable").style.display == "none") {
    document.getElementById("studentTable").style.display = "block";

    let str = '<tr><th>Students</th><th>Score</th>'
    students.forEach(function (student) {

      console.log(student.name + "  / " + student.score);

      str += `<tr><td>${student.name}</td>`;
      str += `<td>${student.score}</td></tr>`;

    });
    str += '</tr>';

    document.getElementById("studentTable").innerHTML = str;
  }
  else
    document.getElementById("studentTable").style.display = "none";
}