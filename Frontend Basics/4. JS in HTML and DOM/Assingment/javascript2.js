const books = [
  { name: "Dune", pages: 412 },
  { name: "The Eye of the World", pages: 782 },
];

window.onload = load;

function load() {
  setInterval(timer, 1000);
}

function addBook() {

  let str = '<li class="list-group-item d-flex justify-content-between lh-sm"><div>';
  str += `<p class="my-0">${document.getElementById('bookName').value}</p>`;
  str += `<small class="text-body-secondary">${document.getElementById('pageCount').value} pages</small>`;
  str += '</div></li>';
  document.getElementById("bookList").innerHTML += str;
}

function timer() {
  let seconds = Number(document.getElementById("seconds").textContent);
  let minutes = Number(document.getElementById("minutes").textContent);

  seconds += 1;
  if (seconds == 60) {
    seconds = 0;
    minutes += 1;
    document.getElementById("minutes").textContent = `${minutes}`;
  }
  document.getElementById("seconds").textContent = `${seconds}`;
}