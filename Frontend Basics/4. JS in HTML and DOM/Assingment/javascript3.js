window.onload = load;

function load() {
    setInterval(timer, 1000);
}


function timer() {
    let seconds = Number(document.getElementById("seconds").textContent);
    let minutes = Number(document.getElementById("minutes").textContent);

    seconds += 1;
    if (seconds == 60) {
        seconds = 0;
        minutes += 1;
        document.getElementById("minutes").textContent = `${minutes}`;
    }
    document.getElementById("seconds").textContent = `${seconds}`;
}