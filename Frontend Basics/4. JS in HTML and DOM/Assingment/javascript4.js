
function postMessage() {

    let username = document.getElementById("username").value;
    let header = document.getElementById("header").value;
    let message = document.getElementById("textArea").value;


    if (!username || !header|| !message) {
        alert("Cannot post ! \nform information missing or incorrect !");
        return;
    }

    let oldUpdates = document.getElementById('updates').innerHTML;
    document.getElementById('updates').innerHTML = newMessage(username, header, message) + oldUpdates;

}

function deleteMessage(event) {
    event.target.parentElement.parentElement.remove();
}


function newMessage(username, header, message) {
    let newMessage = `
        <div class="row pt-3">
            <div class="col-md-4 ms-3"
                style="width: 35px; height: 35px; background: ${random_rgb()}; border-radius: 0.2rem;">
            </div>
            <div class="col-md-8 me-2 p-2 d-flex align-items-center justify-content-start">
                <strong class="d-block text-gray-dark">@${username}</strong>
            </div>
            <h6 class="pt-2">${header}</h6>
            <div class="col-md-12 me-2 p-2 d-flex align-items-center justify-content-start">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    ${message}
                </p>
                <button class="my-3 ms-auto btn btn-danger"
                    onclick="deleteMessage(event)">Delete</button>
            </div>
        </div>
    `
    return newMessage;
}


function random_rgb() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgb(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ')';
}