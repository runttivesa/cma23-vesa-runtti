function fibonacci1(n: number): number {
    if (n <= 1) {
        return n;
    }
    return (fibonacci1(n - 2) * 3) + fibonacci1(n - 1);
}


for (let i = 0; i <= 17; i++)
    console.log(fibonacci1(i));
