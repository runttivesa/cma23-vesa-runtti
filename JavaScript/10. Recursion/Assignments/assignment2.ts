const wordArray = ["The", "quick", "silver", "wolf"];
let words: string = "";

function fibonacci2(n: number): number {
    if (n <= 1) {
        return n;
    }
    return (fibonacci2(n - 1) * 2) + (fibonacci2(2 - n));
}

const sentencify = (start: number, wordArray: string[]) => {
    for (let i = start; i < wordArray.length; i++) {

        if (i == wordArray.length - 1)
            words += `${wordArray[fibonacci2(i)]} !`
        else
            words += `${wordArray[fibonacci2(i)]} `
    }

    return words;
}

console.log(sentencify(2, wordArray));