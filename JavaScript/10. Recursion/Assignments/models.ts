export class Game {
  id: number;
  date: string;
  score: number;
  won: boolean;
}

export class Student {
  name: string;
  score: number;
}

export class ArrayObject {
  x: number
  y: number
  type: string
  toDelete: boolean
}

export class StudentGrade {
  // variables
  name: string
  grade: number

  // constructors
  constructor(name: string, grade: number) {
    this.name = name
    this.grade = grade
  }
}