type Score = {
    S: number,
    A: number,
    B: number,
    C: number,
    D: number,
    F: number,
};

const lookup: Score = {
    S: 8,
    A: 6,
    B: 4,
    C: 3,
    D: 2,
    F: 0,
}

const calculateTotalScore = (scoreArray: string | string[]) => {

    if (typeof (scoreArray) == "string") {
        let totalScore: number = 0;
        for (const letter of scoreArray) {
            totalScore += lookup[letter as keyof Score];
        }
        return totalScore;
    }
    else {

        let gradeSequences: number[] = [];

        for (let i = 0; i < scoreArray.length; i++) {
            let totalScore: number = 0;
            for (const letter of scoreArray[i]) {
                totalScore += lookup[letter as keyof Score];
            }
            gradeSequences[i] = totalScore / scoreArray[i].length;
        }

        return gradeSequences;
    }
}

console.log("---- Section A ----")
let totalScore: number | number[] = calculateTotalScore("DFCBDABSB");
console.log(`total score is: ${totalScore}`);

console.log("\n")

console.log("---- Section B ----")
console.log(`average score is: ${Number(totalScore) / "DFCBDABSB".length}`);

console.log("\n")

console.log("---- Section C ----")
totalScore = calculateTotalScore(["AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC"]);
console.log(`total score is: ${totalScore} `);