const translations = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    string: "merkkijono",
    network: "verkko"
}



const printTranslatableWords = () => {

    Object.keys(translations).forEach((key: string, value: number) => {
        console.log(key + "   /   " + Object.values(translations)[value]);
    })
}

const translate = (word: string) => {

    let message: string = "";

    Object.keys(translations).forEach((key: string, value: number) => {
        if (key == word) {
            message = `${word} translation is: ${Object.values(translations)[value]}`;
        }
        else {
            message = null
        }
    })

    if (message == null)
        console.log("No translation exists for word word given as the argument")

    return message;
}

console.log("\n")

console.log("---- Section A/B ----");
printTranslatableWords();

console.log("\n")

console.log("---- Section C/D ----");
console.log(translate("network"));

console.log("\n")