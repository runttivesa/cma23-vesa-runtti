// interface F {
//     [key: string]:
//     Function
// };

// const commandHandlersTest: F = {
//     "myMethod": (): void => {
//         console.log("WOOOW");
//     }
// }

export interface Coordinate {
    [key: string]: number
};

const commandHandlers = {
    addY: (position: Coordinate) => { position.y++ },
    addX: (position: Coordinate) => { position.x++ },
    subtractY: (position: Coordinate) => { position.y-- },
    subtractX: (position: Coordinate) => { position.x-- },
}

const getPosition = (commandList: string, position: Coordinate) => {
    for (let i = 0; i < commandList.length; i++) {
        if (commandList[i] == "N")
            commandHandlers.addY(position);
        else if (commandList[i] == "E")
            commandHandlers.addX(position);
        else if (commandList[i] == "S")
            commandHandlers.subtractY(position);
        else if (commandList[i] == "W")
            commandHandlers.subtractX(position);
        else if (commandList[i] == "C")
            continue;
        else if (commandList[i] == "B")
            break;
    }
    return position;
}

let position: Coordinate = {
    x: 0,
    y: 0
};

console.log(getPosition("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE", position));