class Room {
    width: number
    height: number
    furniture: string[] = []

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    getArea() {
        return this.width * this.height
    }

    addFurniture(object: string) {
        this.furniture.push(object);
    }
}

console.log("\n");
const room = new Room(4.5, 6.0);
const area = room.getArea();
console.log(`Room area is: ${area}`); // prints 27
console.log("\n");
room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room); // prints Room { width: 4.5, height: 6, furniture: [ 'sofa', 'bed', 'chair' ] }
console.log("\n");