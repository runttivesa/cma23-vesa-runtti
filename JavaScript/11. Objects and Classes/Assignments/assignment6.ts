export interface Coordinate {
    [key: string]: number
};

class Robot {
    position: Coordinate

    constructor(position: Coordinate) {
        this.position = position;
    }

    commandHandlers = {
        addY: (position: Coordinate) => { position.y++ },
        addX: (position: Coordinate) => { position.x++ },
        subtractY: (position: Coordinate) => { position.y-- },
        subtractX: (position: Coordinate) => { position.x-- },
    }

    getPosition(commandList: string) {
        for (let i = 0; i < commandList.length; i++) {
            if (commandList[i] == "N")
                this.commandHandlers.addY(this.position);
            else if (commandList[i] == "E")
                this.commandHandlers.addX(this.position);
            else if (commandList[i] == "S")
                this.commandHandlers.subtractY(this.position);
            else if (commandList[i] == "W")
                this.commandHandlers.subtractX(this.position);
            else if (commandList[i] == "C")
                continue;
            else if (commandList[i] == "B")
                break;
        }

        return this.position;
    }
}

let currentPosition = { x: 0, y: 0 };
let position = new Robot(currentPosition).getPosition("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
console.log(position)