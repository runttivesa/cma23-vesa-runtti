class Animal {
  weight: number
  cuteness: number


  constructor(weight: number, cuteness: number) {
    this.weight = weight;
    this.cuteness = cuteness;
  }

  makeSound() {
    console.log("silence")
  }
}


class Dog extends Animal {
  breed: string

  constructor(weight: number, cuteness: number, breed: string) {
    super(weight, cuteness);
    this.breed = breed;
  }

  override makeSound(): void {
    if (this.cuteness > 4)
      console.log("Awoo");
    else
      console.log("Bark");
  }
}

class Cat extends Animal {

  constructor(weight: number, cuteness: number) {
    super(weight, cuteness);
  }

  override makeSound(): void {
    console.log("Meow");
  }
}

console.log("\n");

console.log("---- Section A ----");
const animal = new Animal(6.5, 4.0);
animal.makeSound();  // prints "silence"
console.log(animal); // prints "Animal { weight: 6.5, cuteness: 4 }"
console.log("\n");

console.log("---- Section B ----");
const cat = new Cat(4.5, 3.0);
cat.makeSound();  // prints "meow"
console.log(cat); // prints "Cat { weight: 4.5, cuteness: 3 }"

console.log("\n");

console.log("---- Section B ----");
const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"


console.log("\n");