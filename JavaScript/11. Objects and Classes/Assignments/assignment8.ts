

class WeatherEvent {
  timestamp: string

  constructor(timestamp: string) {
    this.timestamp = timestamp
  }


  getInformation() {
    return "";
  }
  print() {
    console.log(`${this.timestamp} / ${this.getInformation()}`);
  }
}


class TemperatureChangeEvent extends WeatherEvent {
  temperature: number

  constructor(timestamp: string, temperature: number) {
    super(timestamp);
    this.temperature = temperature;
  }

  override getInformation(): string {
    return "temperature: " + this.temperature.toString() + "°C";
  }
}

class HumidityChangeEvent extends WeatherEvent {
  humidity: number

  constructor(timestamp: string, humidity: number) {
    super(timestamp);
    this.humidity = humidity;
  }

  override getInformation(): string {
    return "humidity: " + this.humidity.toString() + "%";
  }
}

class WindStrengthChangeEvent extends WeatherEvent {
  wind: number

  constructor(timestamp: string, wind: number) {
    super(timestamp);
    this.wind = wind;
  }

  override getInformation(): string {
    return "wind: " + this.wind.toString() + "m/s";
  }
}

const weatherEvents = [];

weatherEvents.push(new TemperatureChangeEvent("2022-11-29 03:00", -6.4));
weatherEvents.push(new HumidityChangeEvent("2022-11-29 04:00", 95));
weatherEvents.push(new WindStrengthChangeEvent("2022-11-30 13:00", 2.2));

weatherEvents.forEach(weatherEvent => weatherEvent.print());