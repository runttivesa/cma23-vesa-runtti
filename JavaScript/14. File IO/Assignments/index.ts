import * as readline from "readline-sync";

function repeat(func: Function, times: number) {
  var promise = Promise.resolve();
  while (times-- > 0) promise = promise.then(() => func());
  return promise;
}

function oneSecond() {
  return new Promise<void>(function (resolve, reject) {
    setTimeout(function () {
      process.stdout.write("-");
      resolve();
    }, 1000);
  });
}

interface Command {
  [key: string]: Function
};

interface Weather {
  cloudy: string;
  sunny: string;
  wind: string;
  temperature: number
  temperatureRange: number[]
}

class User {
  name: string
  actionLog: {
    state: string,
    question: string,
    command: string
  }[] = [];
}

enum State {
  initialize,
  enterCommand,
  help,
  hello,
  helloMessage,
  botInfo,
  botName,
  botRename,
  forecast,
  quit
}

class Bot {
  active: boolean
  user: User;
  state: number;
  command: string;
  question: string;
  name: string;
  weather: Weather;

  constructor(active: boolean, state:number, name:string) {
    this.active = active;
    this.state = state;
    this.name = name
    this.question = "";
    this.weather = <Weather>{
      cloudy: "",
      sunny: "",
      wind: "",
      temperature: 0,
      temperatureRange: [10, 28]
    };
  }

  /* Bot state actions */
  Commands: Command = {
    initialize: () => {
      console.log("\n");
      process.stdout.write("Loading");
      repeat(oneSecond, 3).then(() => {
        this.user = new User();
        this.changeState(State.enterCommand);
      })
    },
    enterCommand: () => {
      console.log("\n");
      console.log("Type 'help' to show commands");
      this.getCommand("Enter a command: ");
      this.changeState(this.state);
    },
    help: () => {
      this.command = "Here´s a list of commands that I can execute!"
      console.log("\n");
      console.log("-----------------------------")
      console.log(this.command)
      console.log("\n");
      console.log("help: Opens this dialog.")
      console.log("hello: I will say hello to you")
      console.log("botInfo: I will introduce myself")
      console.log("botName: I will tell my name")
      console.log("botRename: You can rename me")
      console.log("forecast: I will forecast tomorrows weather 100% accurately")
      console.log("quit: Quits the program.")
      console.log("-----------------------------")
      this.logData();
      this.changeState(State.enterCommand)
    },
    hello: () => {
      console.log("\n");
      this.question = "What is your name";
      this.getCommand(`${this.question} :`);
      this.user.name = this.command;
      this.logData();
      this.changeState(State.helloMessage)
    },
    helloMessage: () => {
      console.log("\n")
      this.command = `Hello there, ${this.user.name}`;
      console.log(`Hello there, ${this.user.name}`);
      this.logData();
      this.changeState(State.enterCommand)
    },
    botInfo: () => {
      console.log("\n");
      for (let i = 0; i < this.user.actionLog.length; i++) {
        let q: string = "";
        if (this.user.actionLog[i].question != "") {
          q = `Question: ${this.user.actionLog[i].question}`;
        }
        console.log(`Bot State: ${this.user.actionLog[i].state} / ${q} -> Reponse: ${this.user.actionLog[i].command}`)
      }
      console.log("\n");
      console.log(`I am a dumb bot. You can ask me almost anything :). You have already asked me n questions.`);
      this.logData();
      this.changeState(State.enterCommand)
    },
    botName: () => {
      console.log("\n");
      this.command = `My name is currently current ${this.name}. If you want to change it, type botRename`;
      console.log(this.command);
      this.logData();
      this.changeState(State.enterCommand);
    },
    botRename: () => {
      console.log("\n");
      this.question = `Type my new name, please.`;
      let newName: string = this.getCommand(`${this.question} \n `);
      this.logData();
      this.question = `Are you happy with the name ${this.command} answer with yes or no?`;
      this.getCommand(`${this.question} \n `);
      this.logData();
      if (this.command == "yes") {
        this.name = newName;
      }
      else {
        console.log(`Name not changed. My name is ${this.name}`);
      }
      this.changeState(State.enterCommand);
    },
    forecast: () => {
      this.generateForecast();
      console.log("\n");
      this.command = `Tomorrows weather will be....`;
      console.log(this.command);
      console.log(`Temperature: ${this.weather.temperature}`);
      console.log(`Cloudy: ${this.weather.cloudy}`);
      console.log(`Sunny: ${this.weather.sunny}`);
      console.log(`Wind: ${this.weather.wind}`);
      this.logData();
      this.changeState(State.enterCommand)
    },
    quit: () => {
      this.active = false;
    }
  }

  generateForecast = () => {
    Object.keys(this.weather).forEach(key => {
      const wKey = key as keyof Weather
      if (key != "temperature") {
        if (Math.random() > 0.5) {
          (this.weather[wKey] as string) = "yes";
        }
        else {
          (this.weather[wKey] as string) = "no";
        }
      }
      else {
        let min: number = this.weather["temperatureRange"][0];
        let max: number = this.weather["temperatureRange"][1];
        (this.weather[wKey] as number) = Math.floor(Math.random() * (max - min + 1) + min);
      }
    });
  }

  /* Logs user question and answer data */
  logData = () => {
    this.user.actionLog.push({ state: State[this.state], question: this.question, command: this.command });
  }

  /* Get player inputed command */
  getCommand = (question: string) => {
    this.command = readline.question(question);
    console.log("this command " + this.command)
    if (this.command == "" || State[this.command as keyof typeof State] == undefined)
      return;
    this.logData();
    let stateID: number = State[this.command as keyof typeof State];
    this.state = stateID;
    this.cleanCommandData();
    return this.command;
  }

  /* Change bot state/action */
  changeState = (newState: State) => {
    this.cleanCommandData();
    this.state = newState;
    this.Commands[State[newState]]();
  }

  /* Clear command and question data */
  cleanCommandData = () => {
    this.command = "";
    this.question = "";
  }
}

const bot: Bot = new Bot(true, 0, "Pelti kuutio");

if (bot.active) {
  bot.Commands[State[bot.state]]();
}