var fs = require('fs');

let string = "";

fs.writeFile('JavaScript/14. File IO/Exercises/log.txt', 'Jouluna poroilla on heinää !', function (err) {
    if (err) throw err;
    console.log('Saved!');
});

// Use fs.readFile() method to read the file
fs.readFile('JavaScript/14. File IO/Exercises/log.txt', 'utf8', function (err, data) {
    
    // Display the file content
    console.log(data);
    
    string = data.replace("poroilla", "lapsilla").replace("heinää", "kinkkua");

    fs.appendFile('JavaScript/14. File IO/Exercises/log.txt', ` ${string}`, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
});