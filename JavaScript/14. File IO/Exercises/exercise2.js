const fs = require("fs");

// 1. get the json data
// This is string data
const fileData = fs.readFileSync("JavaScript/14. File IO/Exercises/data.json", "utf8")
// Use JSON.parse to convert string to JSON Object
const jsonData = JSON.parse(fileData)

// 2. update the value of one key
jsonData["forecast"]["day"] = "online"

console.log(jsonData["forecast"]["day"]);

// 3. write it back to your json file
fs.writeFileSync("JavaScript/14. File IO/Exercises/data.json", JSON.stringify(jsonData))