const sum = (itrations: number) => {

  let totalSum: number = 0;

  for (let i = 0; i <= itrations; i++) {
    totalSum += i

  }

  return totalSum;
}


const sumPromise = async (itrations: number) => {

  let totalSum: number = 0;

  let promise = await new Promise((resolve) => {
    for (let i = 0; i <= itrations; i++) {
      totalSum += i
    }
    resolve(totalSum);
  })
  return promise
}


const sumDelay = async (itrations: number) => {
  let promise = await new Promise((resolve) => {
    new Promise((resolve) => {
      setTimeout(() => {
        return resolve("message || defaultMessage")
      }, 2000)
    }).then(() => {
      let totalSum: number = 0;

      for (let i = 0; i <= itrations; i++) {
        totalSum += i
      }
      return resolve(totalSum);
    })
  })
  return promise;
}


const createDelayedCalculation = async (itrations: number, delay: number) => {
  let promise = await new Promise((resolve) => {
    new Promise((resolve) => {
      setTimeout(() => {
        return resolve("message || defaultMessage")
      }, delay)
    }).then(() => {

      let totalSum: number = 0;

      for (let i = 0; i <= itrations; i++) {
        totalSum += i
      }
      return resolve(totalSum);
    })
  })
  return promise;
}


console.log("\n");
console.log("---- Section E ----")
console.log("Javascript uses event loop due being single threaded programming language, Timeout indicates functions wait time");
console.log("before it is added to execution queue which determines in which order functions will be run");

console.log(sum(10));

sumPromise(50000).then(result => console.log(result));

sumDelay(50000).then(result => console.log(result));

createDelayedCalculation(20000000, 2000).then(result => console.log(result));

createDelayedCalculation(50000, 500).then(result => console.log(result));