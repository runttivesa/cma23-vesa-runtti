
const waitFor = async (delay: number) => {

  await new Promise((resolve) => {
    setTimeout(function () {
      resolve('Time waited')
    }, delay);
  }).then((res) => {
    console.log(res)
  });
}

waitFor(2000);