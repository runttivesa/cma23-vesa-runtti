const waitFor2 = async (delay: number) => {

    await new Promise((resolve) => {
        setTimeout(function () {
            resolve('Time waited')
        }, delay);
    }).then((res) => {
        console.log(res)
    });
}

const countSeconds = async () => {

    for (let i = 0; i <= 10; i++) {
        await waitFor2(1000).then(() => {
            console.log(i);
        });
    }
}

countSeconds();