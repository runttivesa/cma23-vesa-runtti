const axios = require('axios');
function getUniversities() {
  const options = {
    method: 'GET',
    url: 'http://universities.hipolabs.com/search?country=Finland',
    params: {},
    headers: {},
  };
  axios
    .request(options)
    .then((response: any) => {

      console.log("\n");
      console.log("---- Section A ----");
      console.log(response.data);

      console.log("\n");
      console.log("---- Section B ----");
      const universities = response.data.map((x:any) => {
        return x.name;
      });

      console.log(universities);
    })
    .catch((error: any) => {
      console.error(error);
    });
}

getUniversities();