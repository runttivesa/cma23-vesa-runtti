
const axios = require('axios');
function getFakeStoreProducts() {
  const options = {
    method: 'GET',
    url: 'https://fakestoreapi.com/products',
    params: {},
    headers: {},
  };
  axios
    .request(options)
    .then((response: any) => {
      console.log("\n");
      console.log("--- Section A ----");
      Object.keys(response.data).forEach(key => {
        console.log(response.data[key].title);
      })
    })
    .catch((error: any) => {
      console.error(error);
    });
}

function addFakeStoreProduct() {
  const options = {
    method: 'POST',
    url: 'https://fakestoreapi.com/products',
    params: {
      title: 'test product',
      price: 13.5,
      description: 'lorem ipsum set',
      image: 'https://i.pravatar.cc',
      category: 'electronic'
    },
    headers: {},
  };
  axios
    .request(options)
    .then((response: any) => {
      console.log("\n");
      console.log("--- Section B ----");
      console.log(response.data);
    })
    .catch((error: any) => {
      console.error(error);
    });
}

function deleteFakeStoreProduct() {
  const options = {
    method: 'DELETE',
    url: 'https://fakestoreapi.com/products/6',
    params: {},
    headers: {},
  };
  axios
    .request(options)
    .then((response: any) => {
      console.log("\n");
      console.log("--- Section C ----");
      console.log(response.data);
    })
    .catch((error: any) => {
      console.error(error);
    });
}


getFakeStoreProducts();

addFakeStoreProduct();

deleteFakeStoreProduct();  