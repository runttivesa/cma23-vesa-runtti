/* Variables */
const price = 6.5;
let increase = 5;
let result;


console.log("Section A");
console.log("----------------------------");

result = increase + price

console.log(`Orginal price: ${price}`);
console.log(`Increase: ${increase}`);
console.log(`Final price: ${result}`);


console.log("\n")


console.log("Section B");
console.log("----------------------------");

console.log(`Orginal price: ${price}`);
console.log(`Increase: ${increase}`);
console.log(`Final price: ${price+increase}`);