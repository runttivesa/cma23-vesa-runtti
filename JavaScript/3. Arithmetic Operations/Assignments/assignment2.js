/* Variables */
let distance = 120;
let speed = 50;

let result = distance / speed;
const hours = Math.trunc(result)
const minutes = result - Math.floor(result);

console.log(`Distance ${distance}km`);
console.log(`Speed ${speed}km/h`);
console.log(`Hours: ${hours}, Minutes ${Math.round(60 * minutes)}`);