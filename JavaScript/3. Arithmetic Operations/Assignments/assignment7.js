const totalCandies = 100;
const persons = 6;

let candyPerPerson = Math.trunc(totalCandies / persons);
let extraCandy = 1;
let yourCandies = candyPerPerson + extraCandy;

console.log(`You got ${yourCandies % candyPerPerson} extra candies`);
console.log(`Each employee gets ${Math.trunc((100 - extraCandy) / 6)}`);
