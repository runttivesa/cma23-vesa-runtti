
console.log("---- Section A ----")
let number;
const result1 = 10 + number;

// Number variable is not assigned so its type is undefined which causes NaN result"
console.log(`result1 is:${result1} / Number variable is assigned as null so its counted as being 0 which results 10`);

number = null;
const result2 = 10 + number;

// Number variable is assigned as null so its counted as being 0 which results 10
console.log(`result2 is:${result2} / Number variable is assigned as null so its counted as being 0 which results 10`);

console.log("\n");

console.log("---- Section B ----")

const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;

// a variable is a true bool so its value is 1 which is added to 10
console.log(`d value is: ${d} / a variable is a true bool so its value is 1 which is added to 10`);
// b variable is a false bool so its value is 0 which is added to 10
console.log(`e value is: ${e} / b variable is a false bool so its value is 0 which is added to 10`);
