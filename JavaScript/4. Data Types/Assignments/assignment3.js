
/* Section A */

// Question: What does the code above print?
// Answer: Code below prints a bool value of false because age of person1 is not higher than age of person2

/* Section B */

// Question: What is the type of isFirstPersonOlder?
// Answer: Boolean


const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);


console.log("\n");

console.log("---- Section C ----");
let bestAverage = 0;
let className = "";
const classes = 
{ 
    0: {
        class: [9, 6, 9],
        className: "classA",
        average: 0,
    },
    1: {
        class: [7, 10, 5],
        className: "classB",
        average: 0,
    }
}

Object.keys(classes).forEach(item => {

    let average = 0;

    for (let index = 0; index < classes[item].class.length; index++) {
        average += classes[item].class[index]
    }

    classes[item].average = average / classes[item].class.length;
})


Object.keys(classes).forEach(item => {

    if (classes[item].average > bestAverage) {
        bestAverage = classes[item].average;
        className = classes[item].className;
    }
})

console.log(`Best average was: ${bestAverage} / ${className}`);