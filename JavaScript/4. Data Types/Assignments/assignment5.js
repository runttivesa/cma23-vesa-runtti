
console.log("---- Section A ----")

const ageArray = [20, 30, 27, 44];
let average = 0;

for (let index = 0; index < ageArray.length; index++) {
    console.log(ageArray[index]);
}

console.log("\n");

console.log("---- Section B ----")

for (let index = 0; index < ageArray.length; index++) {
    average += ageArray[index];
}

console.log(average / ageArray.length);