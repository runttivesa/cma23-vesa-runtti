books = {
    0: {
        name: "Dune",
        pageCount: 412,
        read: false
    },
    1: {
        name: "The Eye of the World",
        pageCount: 782,
        read: false
    }
}

console.log("---- Section A ----");

Object.keys(books).forEach( item => {
    console.log(`Name: ${books[item].name} / Pages: ${books[item].pageCount} / Read: ${books[item].read}`)
})

console.log("\n");

console.log("---- Section B ----");
Object.keys(books).forEach( item => {
    books[item].read = true;
    console.log(`Name: ${books[item].name} / Pages: ${books[item].pageCount} / Read: ${books[item].read}`)
})