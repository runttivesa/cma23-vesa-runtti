/* Variables */
const numbers = {
    number1: 8,
    number2: 15,
    number3: 90
}

let largestValue;
let smallestValue;

Object.keys(numbers).forEach(item1 => {
    
    let value = numbers[item1]

    Object.keys(numbers).forEach(item2 => {
        if (value > numbers[item2])
         largestValue = value
        else if (value < numbers[item2])
            smallestValue = value;
        else if (value == numbers[item2] && item1 != item2)
            console.log(`value ${value} and ${numbers[item2]} are equal`);
    })
})

console.log(`largest value is: ${largestValue}`);
console.log(`smallest value is: ${smallestValue}`);