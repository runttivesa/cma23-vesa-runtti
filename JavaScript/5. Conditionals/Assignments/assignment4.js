games = {
    PokemonGo: {
        score: 4,
        hoursPlayed: 16,
        price: 0,
    },
    PrinceOfPersia: {
        score: 5,
        hoursPlayed: 200,
        price: 30,
    },
}



Object.keys(games).forEach(item => {

    if (games[item].price > 0) {
        if (games[item].score == 4 && Math.trunc(games[item].hoursPlayed / games[item].price) >= 4) {
            console.log(`${item} was worth its price`);
            return;
        }
        else if (games[item].score == 5 && Math.trunc(games[item].hoursPlayed / games[item].price) >= 2) {
            console.log(`${item} was worth its price`);
            return;
        }
    }
    else {
        if (games[item].score >= 4 && games[item].price <= 0) {
            console.log(`${item} was worth its price`);
            return;
        }
    }
})