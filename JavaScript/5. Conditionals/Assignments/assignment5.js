const names = ["Maria", "Joe", "Philippa"];

for (let index1 = 0; index1 < names.length; index1++) {
    for (let index2 = 0; index2 < names.length; index2++) {
        if (names[index1] > names[index2]) {
            let orginal = names[index1];
            names[index1] = names[index2];
            names[index2] = orginal;
        }
    }
}