let balance = 100;
let inactive = false;
let checkBalance = true;

if (checkBalance) {
  if (balance > 0 && !inactive) {
    Print(balance);
  }
  else {
    if (inactive) {
      Print("Your account is not active")
    }
    else {
      if (balance == 0) {
        Print("Your account is empty")
      }
      else {
        Print("Your balance is negative")
      }
    }
  }
}
else {
  Print("Have a nice day");
}

function Print(message) {
  console.log(message);
}
