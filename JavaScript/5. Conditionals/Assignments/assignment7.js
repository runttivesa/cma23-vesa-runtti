let fruits = {
    pear: {
        weight: 178
    },
    lemon: {
        weight: 120
    },
    apple: {
        weight: 90
    },
    mango: {
        weight: 150
    }
}
let average = 0;
let closestWeight = Infinity;
let closestFruit = "";


console.log("---- Section A ----");

Object.keys(fruits).forEach( item => {
    average += fruits[item].weight;
})
average = average / Object.keys(fruits).length;
console.log(`Fruits average weight is: ${average}`);


console.log("\n");


console.log("---- Section B ----");

Object.keys(fruits).forEach( item => {

    if (Math.abs(fruits[item].weight - average) < closestWeight) {
        closestWeight = Math.abs(fruits[item].weight - average);
        closestFruit = item;
    }

})

console.log(`Closest to avarage weight is: ${closestFruit.charAt(0).toUpperCase() + closestFruit.slice(1)} / ${closestWeight}`);
