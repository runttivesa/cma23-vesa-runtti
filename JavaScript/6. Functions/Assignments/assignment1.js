/* Variables */
let language = "es";
let localization = {
    fi: {
        hello: "Hei maailma"
    },
    en: {
        hello: "Hello World"
    },
    es: {
        hello: "Hola Mundo"
    }
}

/* Functions */
const helloNoParam = () => {
    console.log(localization[language].hello);
}

const helloParam = (selectedLanguage) => {
    console.log(localization[selectedLanguage].hello);
}


console.log("---- Section A ----");
helloNoParam();

console.log("\n");

console.log("---- Section B ----");
helloNoParam(language);