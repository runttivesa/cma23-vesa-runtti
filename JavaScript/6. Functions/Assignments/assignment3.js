const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

let array = [];

const compare = (array) => {
    let largestArea = 0;

    array.forEach(element => {
        if (element > largestArea)
            largestArea = element;
    });

    console.log(largestArea);
}

const calculateArea = ( shape, id, width, length, divider) => {

    const area = divider != null ? width * length / 2 : width * length;
    array.push(area);

    console.log(`Area of ${shape} ${id} is: ${area}`);
}

calculateArea("triangle", "1", firstTriangle.width, firstTriangle.length, 2);
calculateArea("triangle", "2", secondTriangle.width, secondTriangle.length, 2);
calculateArea("triangle", "3", thirdTriangle.width, thirdTriangle.length, 2);
compare(array);