const ages = [20, 35, 27, 44, 32];


const printAges = (array) => {

    let averageAge = 0;

    for (let i = 0; i < array.length; i++) {
        console.log(`student ${i} age is: ${ages[i]}`);
        averageAge += ages[i];
    }

    averageAge /= array.length;

    console.log(`students average age is: ${averageAge}`);
}



printAges(ages);