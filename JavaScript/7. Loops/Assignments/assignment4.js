const vowels = {
    a: {
        count: 0
    },
    e: {
        count: 0
    },
    i: {
        count: 0
    },
    o: {
        count: 0
    },
    u: {
        count: 0
    },
    y: {
        count: 0
    },
}

const countVowels = (sentence) => {
    [...sentence].forEach(item => {
        Object.keys(vowels).forEach(vowel => {
            if (item.toLowerCase() == vowel) {
                vowels[vowel].count += 1;
            }
        });
    })

    let totalCount = 0;
    Object.keys(vowels).forEach(item => {
        console.log(`${item.toLocaleUpperCase()} letter count: ${vowels[item].count}`)
        totalCount += vowels[item].count;
    });

    console.log("Total vowel count: " + totalCount);
}

countVowels("A wizard's job is to vex chumps quickly in fog.");