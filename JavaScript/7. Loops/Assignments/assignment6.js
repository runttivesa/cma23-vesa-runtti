let x = y = 0;
const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

const calculatePosition = () => {

    for (let i = 0; i < commandList.length; i++) {

        let input = commandList.charAt(i);
        
        if (input == "B")
            break;
        if (input == "C")
            continue;
        else {
            if (input == "N")
                y++;
            else if (input == "E")
                x++;
            else if (input == "S")
                y--;
            else if (input == "W")
                x--;
        }
    }
    console.log(`X : ${x} | Y : ${y}`);
}

calculatePosition();