const isPalindrome = (word) => {
  
  let reversedWord = "";

  for (let i = word.length; i >= 0; i--) {
    reversedWord += word.charAt(i);
  }

  if (reversedWord == word)
    return true;
  else
   return false;
}

let value = isPalindrome("saippuakivikauppias");
console.log(value); // prints true

value = isPalindrome("saippuakäpykauppias");
console.log(value); // prints false