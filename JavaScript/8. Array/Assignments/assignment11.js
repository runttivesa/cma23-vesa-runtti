const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

// Declare array of functions
var arrayOfFunctions = [
    function () {return {y: 1}},
    function () {return {x: 1}},
    function () {return {y: -1}},
    function () {return {x: -1}},
    function () {}
]


const translateCommands = (commands) => {

    let numArray = []
    let coordinates = { x: 0, y: 0 }

    for (const key in commands) {
        if (commands[key] == "N")
            numArray.push(0);
        else if (commands[key] == "E")
            numArray.push(1);
        else if (commands[key] == "S")
            numArray.push(2);
        else if (commands[key] == "W")
            numArray.push(3);
        else if (commands[key] == "C")
            numArray.push(4);
        else if (commands[key] == "B")
            numArray.push(5);
    }

    for (const key of numArray) {

        if (key == 5)
            break;

        // Get movement coordinate
        val = arrayOfFunctions[key]();

        // Assign movement coordinates
        if (val) {
            //console.log(Object.keys(val)[0] + " / " + Object.values(val)[0])
            coordinates[Object.keys(val)[0]] += Object.values(val)[0]
        }
    }

    return coordinates;
}


console.log(translateCommands(commandList));