
const numberRange = (start, end) => {

  let numArray = [];

  if (end > start) {
    for (let i = start; i <= end; i++) {
      numArray.push(i);
    }
  }
  else {
    for (let i = end; i <= start; i++) {
      numArray.push(i);
    }
  }

  console.log(numArray);
}

numberRange(1, 5);
numberRange(-5, -1);
numberRange(9, 5);