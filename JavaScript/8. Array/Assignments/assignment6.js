
const findLargest = (numArray) => {

    let largestValue = 0;

    [...numArray].forEach(item => {
        if (item > largestValue)
            largestValue = item;
    })
    return largestValue;
}

const array = [4, 19, 7, 1, 9, 22, 6, 13];
const largest = findLargest(array);
console.log(largest); // prints 22