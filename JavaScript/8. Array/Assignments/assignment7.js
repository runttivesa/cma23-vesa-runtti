const sortNumberArray = (numArray) => {

  for (let i = 0; i < numArray.length; i++) {
    for (let j = 0; j < numArray.length; j++) {
      if (numArray[i] < numArray[j]) {
        let currentNum = numArray[i];
        numArray[i] = numArray[j];
        numArray[j] = currentNum;
      }
    }
  }
}

const array = [4, 19, 7, 1, 9, 22, 6, 13];
sortNumberArray(array);
console.log(array); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]

