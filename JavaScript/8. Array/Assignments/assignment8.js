const reverseWords = (sentence) => {

  inverseSentence = [];
  
  for (let i = (sentence.length - 1); i >= 0; i--) {
    inverseSentence.push(sentence[i]);
  }

  inverseSentence = inverseSentence.join("").split(" ");
  inverseSentence = inverseSentence.reverse().join(" ");
  
  return inverseSentence
}

const sentence = "this is a short sentence";
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"