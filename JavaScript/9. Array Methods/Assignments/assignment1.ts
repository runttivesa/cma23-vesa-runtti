var numbers: number[] = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50]

console.log("---- Section A ----")

for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] >= 20) {
        console.log(numbers[i]);
        break;
    }
}

console.log("\n");

console.log("---- Section B ----");

const element = numbers.find((element) => element >= 20 );
console.log(element);

console.log("\n");

console.log("---- Section C ----");

const index = numbers.findIndex(element => element >= 20)
console.log(numbers[index]);

console.log("\n");

console.log("---- Section D ----");

numbers.splice(index, (numbers.length - index));

console.log(numbers);