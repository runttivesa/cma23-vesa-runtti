import { games } from "./data";
import { Game } from "./models";

console.log("---- Section A ----")

const game:Game = games.find(element => element.id.toString() == "1958468135")

console.log(game);


console.log("\n")


console.log("---- Section B ----")

const gameWon:Game = games.find(element => element.won == true)

console.log(gameWon);