import { games } from "./data";
import { Game } from "./models";

{
    console.log("---- Section A----")
    const gamesWon: Game[] = games.filter(element => element.won == true)
    let average: number = 0;

    console.log(gamesWon);

    for (const game of gamesWon) {

        average += game.score;
    }
    console.log(`Average of games won is: ${average / gamesWon.length}`);
}


console.log("\n")


{
    console.log("---- Section B----")
    const gamesLost: Game[] = games.filter(element => element.won == false)
    let average: number = 0;
    
    console.log(gamesLost);

    for (const game of gamesLost) {

        average += game.score;
    }
    console.log(`Average of games lost is: ${average / gamesLost.length}`);
}