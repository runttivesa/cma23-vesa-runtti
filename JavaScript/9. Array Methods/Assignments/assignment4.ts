
console.log("---- Section A ----");

const incrementAll = (numbers: number[]) => {

    var tempArray = [];

    for (let value of numbers) {
        value += 1;
        console.log(value);
        //console.log(value)
    }
}

var numbers: number[] = [4, 7, 1, 8, 5];
var newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]

console.log("\n")

console.log("---- Section B ----")

const decrementAll = (numbers: number[]) => {

    var tempArray = [];

    for (let value of numbers) {
        value -= 1;
        console.log(value);
        //console.log(value)
    }
}

var numbers = [4, 7, 1, 8, 5];
var newNumbers = decrementAll(numbers);
console.log(newNumbers); // prints [ 3, 6, 0, 7, 4 ]