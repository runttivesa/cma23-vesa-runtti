import { students } from "./data"
import { Student, StudentGrade } from "./models";

const getGrades = (studentList:Student[]) => {

  var gradeList: StudentGrade[] = [];

  for (const student of studentList) {
    if (student.score < 14.0) {

      var studentGrade = new StudentGrade(student.name, 0)
      gradeList.push(studentGrade);
    }
    else if (student.score >= 14.0 && student.score <= 17.00) {
      var studentGrade = new StudentGrade(student.name, 1)
      gradeList.push(studentGrade);
    }
    else if (student.score >= 17.0 && student.score <= 20.00) {
      var studentGrade = new StudentGrade(student.name, 2)
      gradeList.push(studentGrade);
    }
    else if (student.score >= 20.0 && student.score <= 23.00) {
      var studentGrade = new StudentGrade(student.name, 3)
      gradeList.push(studentGrade);
    }
    else if (student.score >= 23.0 && student.score <= 26.00) {
      var studentGrade = new StudentGrade(student.name, 4)
      gradeList.push(studentGrade);
    }
    else if (student.score >= 26) {
      var studentGrade = new StudentGrade(student.name, 5)
      gradeList.push(studentGrade);
    }
  }

  console.log(gradeList);
}

getGrades(students);