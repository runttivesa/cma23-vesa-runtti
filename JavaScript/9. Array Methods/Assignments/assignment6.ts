import { ArrayObject } from "./models";

const objectArray: ArrayObject[] = [
  { x: 14, y: 21, type: "tree", toDelete: false },
  { x: 1, y: 30, type: "house", toDelete: false },
  { x: 22, y: 10, type: "tree", toDelete: true },
  { x: 5, y: 34, type: "rock", toDelete: true },
  null,
  { x: 19, y: 40, type: "tree", toDelete: false },
  { x: 35, y: 35, type: "house", toDelete: false },
  { x: 19, y: 40, type: "tree", toDelete: true },
  { x: 24, y: 31, type: "rock", toDelete: false }
];

let newArrayA: ArrayObject[] = [...objectArray];
let newArrayB: ArrayObject[] = [...objectArray];

const cleanupA = () => {
  console.log("---- Section A ----")

  let start = performance.now();

  for (let object in newArrayA) {
    if (newArrayA[object] != null)
      if (newArrayA[object].toDelete)
        newArrayA[object] = null;
  }
  let elapsed = performance.now();
  console.log(newArrayA);
  console.log(`Time to run:${elapsed - start}`);
}

const cleanupB = () => {
  console.log("---- Section B ----")

  let start = performance.now();

  newArrayB = newArrayB.map(element => {
    if (element == null)
      return null;
    else
      if (element.toDelete)
        return null;
      else
        return element;
  })

  let elapsed = performance.now();
  console.log(newArrayB);
  console.log(`Time to run:${elapsed - start}`);
}

cleanupA();

console.log("\n")

cleanupB();

console.log("\n")

console.log("---- Section C ----")
console.log("Section B solution is more memory and calculation intensive\ndue it has more parameters it can take which require additional logic for sorting")
