
interface Data {
    [key: string]: number
};

const testFunc = (sentence: string) => {

    let data: Data = {
        words: 0,
        length: 0
    };

    data["words"] = sentence.split(" ").length;

    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] != " ")
            data["length"] += 1
    }

    return data;
}


console.log(testFunc("Test Sentece From The Hell"))