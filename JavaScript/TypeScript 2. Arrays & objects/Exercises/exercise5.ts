
interface Boat {
    hullBreached: boolean;
    fillLevel: number;
    sinked?: boolean,
}

const isItSinking = (breached: boolean, fill: number) => {

    let newBoat: Boat = {
        hullBreached: breached,
        fillLevel: fill
    }


    if (newBoat.hullBreached) {
        while (newBoat.fillLevel < 100) {
            newBoat.fillLevel += 20;
        }
        newBoat.sinked = true;
    }



    return newBoat;
}


console.log(isItSinking(true, 0))