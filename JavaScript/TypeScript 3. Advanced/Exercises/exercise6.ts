var fs = require('fs');

interface Book {
  author: string;
  title: string;
  readingStatus: boolean,
  id: number
}

const library: Book[] = [
  {
    author: 'David Wallace',
    title: 'Infinite Jest',
    readingStatus: false,
    id: 1,
  },
  {
    author: 'Douglas Hofstadter',
    title: 'Gödel, Escher, Bach',
    readingStatus: true,
    id: 2,
  },
  {
    author: 'Harper Lee',
    title: 'To Kill A Mockingbird',
    readingStatus: false,
    id: 3,
  },
];


function getBook(id: number) {

  let bookName: string;

  Object.keys(library).forEach(key => {
    if (library[Number(key)].id == id) {
      bookName = library[Number(key)].title;

    }
  })

  return bookName;
}


function printBookData(id: number) {

  let book: Book;

  Object.keys(library).forEach(key => {
    if (library[Number(key)].id == id) {
      book = {
        author: library[Number(key)].author,
        title: library[Number(key)].title,
        readingStatus: library[Number(key)].readingStatus,
        id: library[Number(key)].id
      }
    }
  })

  return book;
}


function printReadingStatus(author: string, title: string) {

  let readingStatus: boolean;

  Object.keys(library).forEach(key => {
    if (library[Number(key)].author == author ||
      library[Number(key)].author == author) {
      readingStatus = library[Number(key)].readingStatus;
    }
  })

  return readingStatus;
}

function addNewBook(author: string, title: string) {

  const book: Book = {
    author: author,
    title: title,
    readingStatus: false,
    id: library.length + 1
  }

  library.push(book)

  //console.log(library);
}


function readBook(id: number) {

  let readingStatus: boolean;

  Object.keys(library).forEach(key => {
    if (library[Number(key)].id === id) {
      library[Number(key)].readingStatus = true;
    }
  })

  //console.log(library);
}

const saveToJSON = () => {
  fs.writeFileSync("exercise.json", JSON.stringify(library), "utf8");
}


const loadFromJSON = () => {
  const newLibraryString = fs.readFileSync("exercise.json", "utf8");
  const newLibrary = JSON.parse(newLibraryString);
  console.log(newLibrary);
}


console.log(getBook(1));
console.log(printBookData(1))
console.log(printReadingStatus("David Wallace", "Infinite Jest"));
addNewBook("Matti kalassa", "Osa 2");
readBook(4)
saveToJSON();
loadFromJSON();
