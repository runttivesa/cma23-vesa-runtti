const numArrayA: any[] = [
    0,
    2,
    "4",
    "6",
    false
]

const numArrayB: any[] = [
    0,
    2,
    5,
    6,
]

const typeGuard = (value: any): value is number[] => {
    return Array.isArray(value) && value.every(item => typeof item === 'number');
}

console.log(typeGuard(numArrayA))
console.log(typeGuard(numArrayB))