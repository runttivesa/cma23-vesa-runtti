import { useState } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { UserData, UserContext } from "./components/Context";
import Navbar from './components/Navbar';
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import './App.css';

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <>
        <HomePage />
      </>
    ),
  },
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
]);

function App() {

  const [userData, setUserData] = useState<UserData>({
    logged: false,
    id: null,
    username: null,
    rank: null,
  });

  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      <div className="App">
        <div className="main">
          <div className='headerImage' />
          <Navbar />
          <div className="container">
            <RouterProvider router={router} />
          </div>
        </div>
      </div >
    </UserContext.Provider>
  );
}

export default App;
