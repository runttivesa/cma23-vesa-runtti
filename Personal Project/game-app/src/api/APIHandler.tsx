import axios from 'axios';
const baseURL = "http://localhost:8080/";

const AxiosCall = async (props: { method: string, path: string, data: any }) => {

    console.log(props.data);
    try {
        const response = await axios({
            method: props.method,
            url: baseURL + props.path,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: props.data
        });

        //console.log(response.data);
        return response.data;
    }
    catch (error) {
        console.log(error)
    }
}

export default AxiosCall;