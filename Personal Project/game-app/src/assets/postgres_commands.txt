
// POSTGRES CREATE TABLE COMMANDS

CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	role INT NOT NULL,
	rank INT NOT NULL,
	imagePath VARCHAR (255) NOT NULL,
	username VARCHAR (255) NOT NULL,
	password VARCHAR (255) NOT NULL,
	fullname VARCHAR (255) NOT NULL,
	birthdate DATE NOT NULL,
	country VARCHAR (255) NOT NULL,
	createdDate DATE NOT NULL,
	modifiedDate DATE NOT NULL
);


CREATE TABLE reviews (
	id SERIAL PRIMARY KEY,
	userId INT NOT NULL,
	score DECIMAL NOT NULL,
	publishDate DATE NOT NULL,
	createdDate DATE NOT NULL,
	modifiedDate DATE NOT NULL,
	imagePath VARCHAR (255) NOT NULL,
	likes INT NOT NULL,
	dislikes INT NOT NULL,
	gameTitle VARCHAR (255) NOT NULL,
	review TEXT NOT NULL,
	platformId INT NOT NULL,
	genreId INT NOT NULL,
	developer VARCHAR (255) NOT NULL,
	publisher VARCHAR (255) NOT NULL
);


CREATE TABLE genres (
	id SERIAL PRIMARY KEY,
	name VARCHAR (255) NOT NULL
);


CREATE TABLE ranks (
	id SERIAL PRIMARY KEY,
	name VARCHAR (255) NOT NULL
);


CREATE TABLE platform (
	id SERIAL PRIMARY KEY,
	name VARCHAR (255) NOT NULL
);


SELECT id, username, rank FROM users
	WHERE username = 'Harald'
	AND 
	password = 'harald';

SELECT * FROM reviews;