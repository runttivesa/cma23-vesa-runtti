import { createContext } from 'react';

export type UserData = {
  logged: boolean,
  id: number | null,
  username: string | null,
  rank: number | null
}

export interface ContextProps {
  readonly userData: UserData | null;
  readonly setUserData: (userData: UserData) => void;
}
export const UserContext = createContext<ContextProps>({
  userData: null,
  setUserData: () => null,
});