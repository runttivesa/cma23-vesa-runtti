import { useState, Dispatch, SetStateAction } from "react";

export interface ILogin {
  username: string,
  password: string,
}

export const Login = (props: { LoginCall: Function, setShowRegister: Dispatch<SetStateAction<Boolean>> }) => {

  const [loginData, setLoginData] = useState<ILogin>({
    username: "Harald",
    password: "harald"
  })

  return (
    <>
      <div className="col-8 text-white">
        <div className="d-flex justify-content-center align-items-center mx-auto mt-5 mb-5">
          <h5
            className="text-uppercase monomaniac mx-auto mt-5 mb-3 border-bottom"
            style={{ width: "60%" }}
          >
            Login / Register
          </h5>
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            username:
          </label>
          <input
            type="text"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={loginData.username}
            onChange={(event) => setLoginData({ ...loginData, username: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            password:
          </label>
          <input
            type="password"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={loginData.password}
            onChange={(event) => setLoginData({ ...loginData, password: event.target.value })}
          />
        </div>
        <button
          type="button"
          className="btn mt-5"
          style={{ backgroundColor: "#D9D9D9" }}
          onClick={() => props.LoginCall(loginData)}
        >
          login
        </button>
      </div>
      <div
        className="col-8 d-flex justify-content-center align-items-center text-white"
        style={{ height: "140px" }}
      >
        <p className="mt-4">
          <a href="#" className="text-white" onClick={() => props.setShowRegister(true)}>Don't have a Account ?</a>
        </p>
      </div>
    </>
  );
};