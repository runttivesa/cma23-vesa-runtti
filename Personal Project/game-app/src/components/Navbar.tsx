import findIcon from "../assets/images/Find_Icon.png"
import { UserContext } from '../components/Context';
import { useContext } from 'react';

export const Navbar = () => {

  const user = useContext(UserContext);

  const Logout = () => {
    user.setUserData({
      logged: false,
      id: null,
      username: null,
      rank: null
    })
  }

  return (
    <nav
      className="navbar fixed-top navbar-expand-lg navbar-dark m-0 p-0"
      aria-label="Tenth navbar example"
    >
      <div className="container-fluid">

        <div
          className="collapse navbar-collapse justify-content-center"
        >
          <div
            className="d-flex justify-content-center align-items-center trapezoid"
          >
            <ul className="navbar-nav text-uppercase mb-5">
              <li className="nav-item">
                <a
                  className="nav-link active monomaniac"
                  style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                  aria-current="page"
                  href="#"
                >
                  {`> reviews`}
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active monomaniac"
                  style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                  aria-current="page"
                  href="#">{`> ask space pig`}
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active monomaniac"
                  style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                  aria-current="page"
                  href="#"
                >
                  {`> about`}
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active monomaniac"
                  style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                  aria-current="page"
                  href="#"
                >
                  {`> merchandise`}
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="#">
                  <img
                    className="pb-2"
                    src={findIcon}
                    alt=""
                    style={{ objectFit: "cover", height: "30px", width: "100%" }}
                  />
                </a>
              </li>
              {!user.userData?.logged && user.userData?.id === null &&
                <li className="nav-item">
                  <a
                    className="nav-link active monomaniac"
                    style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                    aria-current="page"
                    href="/login"
                  >
                    {`> login/register`}
                  </a>
                </li>
              }
              {user.userData?.logged && user.userData?.id !== null &&
                <li className="nav-item">
                  <a
                    className="nav-link active monomaniac"
                    style={{ fontSize: "14px", letterSpacing: "0.75px" }}
                    aria-current="page"
                    onClick={() => Logout()}
                    href="/"
                  >
                    {`> logout`}
                  </a>
                </li>
              }
            </ul>
          </div>
        </div>
      </div>
    </nav >
  );
};

export default Navbar;