
import { useState, Dispatch, SetStateAction } from "react";

export interface IRegister {
  role: number,
  rank: number,
  imagePath: string,
  username: string,
  password: string,
  fullname: string,
  birthdate: string,
  country: string,
  createdDate: string,
  modifiedDate: string
}

export const Register= (props: { RegisterCall: Function, setShowRegister: Dispatch<SetStateAction<Boolean>> }) => {

  const [registerData, setRegisterData] = useState<IRegister>({
    role: 666,
    rank: 1,
    imagePath: "",
    username: "",
    password: "",
    fullname: "",
    birthdate: `${new Date().toISOString().split('T')[0]}`,
    country: "",
    createdDate: `${new Date().toISOString()}`,
    modifiedDate: `${new Date().toISOString()}`,
  })

  return (
    <>
      <div className="col-8 text-white">
        <div className="d-flex justify-content-center align-items-center mx-auto mt-5 mb-5">
          <h5
            className="text-uppercase monomaniac mx-auto mt-5 mb-3 border-bottom"
            style={{ width: "60%" }}
          >
            Login / Register
          </h5>
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            username:
          </label>
          <input
            type="text"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={registerData.username}
            onChange={(event) => setRegisterData({ ...registerData, username: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            password:
          </label>
          <input
            type="password"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={registerData.password}
            onChange={(event) => setRegisterData({ ...registerData, password: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            fullname:
          </label>
          <input
            type="text"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={registerData.fullname}
            onChange={(event) => setRegisterData({ ...registerData, fullname: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            birthdate:
          </label>
          <input
            type="date"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={registerData.birthdate}
            onChange={(event) => setRegisterData({ ...registerData, birthdate: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            country:
          </label>
          <input
            type="text"
            className="input form-control-sm ms-auto w-50 monomaniac"
            value={registerData.country}
            onChange={(event) => setRegisterData({ ...registerData, country: event.target.value })}
          />
        </div>
        <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
          <label
            className="maretha my-auto text-nowrap"
            style={{ fontSize: "1.25rem" }}
          >
            role:
          </label>
          <select
            className="form-select form-select-sm ms-auto w-50 maretha"
            aria-label=".form-select example"
            value={registerData.role}
            onChange={(event) => setRegisterData({ ...registerData, role: Number(event.target.value) })}
          >
            <option value="666" disabled>role</option>
            <option value="0">admin</option>
            <option value="1">reviewer</option>
          </select>
        </div>
        <button
          type="button"
          className="btn mt-5"
          style={{ backgroundColor: "#D9D9D9" }}
          onClick={() => props.RegisterCall(registerData)}
        >
          Register
        </button>
      </div >
      <div
        className="col-8 d-flex justify-content-center align-items-center text-white"
        style={{ height: "140px" }}
      >
        <p className="mt-4">
          <a href="#" className="text-white" onClick={() => props.setShowRegister(false)}>{`< Back`}</a>
        </p>
      </div>
    </>
  );
};