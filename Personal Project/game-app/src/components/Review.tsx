import { useState, Dispatch, SetStateAction } from 'react';

export interface IReview {
  userid: number | null,
  score: number,
  publishdate: string,
  createddate: string,
  modifieddate: string,
  imagepath: string,
  likes: number,
  dislikes: number,
  gametitle: string,
  review: string,
  platformid: number,
  genreid: number,
  developer: string,
  publisher: string
}

export const Review = (props: { ReviewCall: Function, setModalActive: Dispatch<SetStateAction<Boolean>>, userId: number }) => {

  const [reviewData, setReviewData] = useState<IReview>({
    userid: props.userId,
    score: 5,
    publishdate: `${new Date().toISOString().split('T')[0]}`,
    createddate: `${new Date().toISOString()}`,
    modifieddate: `${new Date().toISOString()}`,
    imagepath: "../assets/images/tarkov.jpg",
    likes: 0,
    dislikes: 0,
    gametitle: "",
    review: "This is my review...",
    platformid: 666,
    genreid: 666,
    developer: "",
    publisher: ""
  });

  console.log(reviewData);

  return (
    <div
      className="col-6 position-fixed d-flex flex-column justify-content-center align-items-center rounded-4"
      style={{ top: "50%", left: "50%", transform: "translate(-50%, -50%)", zIndex: 2, backgroundColor: "#9e9e9e" }}
    >
      <>
        <div className="col-8 text-white">
          <div className="d-flex justify-content-center align-items-center mx-auto mt-5 mb-5">
            <h5
              className="text-uppercase monomaniac mx-auto mt-5 mb-3 border-bottom"
              style={{ width: "60%" }}
            >
              write new review
            </h5>
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              score:
            </label>
            <div className="d-flex flex-row w-50 ms-auto">
              <input
                type="range"
                value={reviewData.score}
                min="0"
                max="10"
                step="0.1"
                className="w-75 form-range"
                id="customRange1"
                onChange={(event) => setReviewData({ ...reviewData, score: Number(event.target.value) })}
              >
              </input>
              <div className="w-25">
                <div className="w-75 ms-auto bg-white rounded-5 text-black">
                  {reviewData.score}
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              publish date:
            </label>
            <input
              type="date"
              className="input form-control-sm ms-auto w-50 monomaniac"
              value={reviewData.publishdate}
              onChange={(event) => setReviewData({ ...reviewData, publishdate: event.target.value })}
            />
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              game title:
            </label>
            <input
              type="text"
              className="input form-control-sm ms-auto w-50 monomaniac"
              value={reviewData.gametitle}
              onChange={(event) => setReviewData({ ...reviewData, gametitle: event.target.value })}
            />
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              plaform:
            </label>
            <select
              className="form-select form-select-sm ms-auto w-50 maretha"
              aria-label=".form-select example"
              value={reviewData.platformid}
              onChange={(event) => setReviewData({ ...reviewData, platformid: Number(event.target.value) })}
            >
              <option value="666" disabled>plaform</option>
              <option value="0">pc</option>
              <option value="1">pc:mod</option>
              <option value="2">xbox</option>
              <option value="3">playstation 4</option>
              <option value="4">playstaion 3</option>
              <option value="5">xbox series x</option>
              <option value="6">xbox one</option>
              <option value="7">switch</option>
            </select>
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              genre:
            </label>
            <select
              className="form-select form-select-sm ms-auto w-50 maretha"
              aria-label=".form-select example"
              value={reviewData.genreid}
              onChange={(event) => setReviewData({ ...reviewData, genreid: Number(event.target.value) })}
            >
              <option value="666" disabled>genre</option>
              <option value="1">action</option>
              <option value="2">rpg</option>
              <option value="3">puzzle</option>
              <option value="4">platformer</option>
              <option value="5">horror</option>
            </select>
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              developer:
            </label>
            <input
              type="text"
              className="input form-control-sm ms-auto w-50 monomaniac"
              value={reviewData.developer}
              onChange={(event) => setReviewData({ ...reviewData, developer: event.target.value })}
            />
          </div>
          <div className="d-flex justify-content-center align-items-center mx-auto mt-4 mb-4">
            <label
              className="maretha my-auto text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              publisher:
            </label>
            <input
              type="text"
              className="input form-control-sm ms-auto w-50 monomaniac"
              value={reviewData.publisher}
              onChange={(event) => setReviewData({ ...reviewData, publisher: event.target.value })}
            />
          </div>
          <div className="mb-3 mt-5 border-top">
            <label
              className="maretha mt-5 text-nowrap"
              style={{ fontSize: "1.25rem" }}
            >
              review:
            </label>
            <textarea
              value={reviewData.review}
              className="form-control mt-4"
              style={{ minHeight: "200px" }}
              id="exampleFormControlTextarea1"
              onChange={(event) => setReviewData({ ...reviewData, review: event.target.value })}
            >
            </textarea>
          </div>
          <button
            type="button"
            className="btn mt-5"
            style={{ backgroundColor: "#D9D9D9" }}
            onClick={() => props.ReviewCall(reviewData)}
          >
            submit review
          </button>
        </div>
        <div
          className="col-8 d-flex justify-content-center align-items-center text-white"
          style={{ height: "140px" }}
        >
          <h5
            className="mt-4 text-white text-underline"
            onClick={() => props.setModalActive(false)}
          >
            {`< Back`}
          </h5>
        </div>
      </>
    </div >
  )
};