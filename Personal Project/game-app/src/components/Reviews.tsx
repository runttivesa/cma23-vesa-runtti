import { useContext, useState, Dispatch, SetStateAction, useEffect } from 'react';
import { UserContext } from '../components/Context';
import AxiosCall from '../api/APIHandler';
import { Review, IReview } from './Review';
import reviewerIcon from "../assets/images/Reviewer_Icon.jpeg"
import clockIcon from "../assets/images/Clock_Icon.png"
import chatIcon from "../assets/images/Chat_Icon.png"
import cannedIcon from "../assets/images/Canned_Icon.png"
import anomalie01 from "../assets/images/Anomalie_Image_01.png"


const ReviewCardAlt = (props: { review: IReview }) => {

  interface IReviewer {
    username: string | undefined,
    rank: number | undefined,
  }

  const [reviewer, setReviewer] = useState<IReviewer>();
  const [render, setRender] = useState<boolean>(false);

  useEffect(() => {
    // declare the async data fetching function
    const fetchData = async () => {
      // get the data from the api
      const data = await AxiosCall({ method: "GET", path: `getUser/${props.review.userid}`, data: "" });

      // set state with the result
      //console.log(data.rows[0]);
      setReviewer(data.rows[0])
    }

    // call the function
    fetchData()
      // make sure to catch any error
      .catch(console.error);

  }, [props.review.userid])

  return (
    <div className="col-12 text-white my-3">
      <div
        className="col-12 m-0 p-0 d-flex flex-row justify-content-center align-items-center rounded-2"
        style={{ height: "80px", backgroundColor: "#1E1E1E", border: "2px solid rgba(0, 255, 255, 0.2)" }}
      >

        <div className="col-4 d-flex h-100 flex-row">
          <div className="col-4 h-100  d-flex justify-content-center align-items-center">
            <div
              className="d-flex justify-content-center align-items-center bg-success"
              style={{ borderRadius: "60px", height: "50px", width: "50px" }}
            >
              <h4
                className="m-0 p-0"
                style={{ lineHeight: 0 }}
              >
                {props.review.score}
              </h4>
            </div>
          </div>
          <div className="col-8 d-flex justify-content-start align-items-center ms-auto"
            style={{ overflow: "hidden" }}>
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={""}
              alt=""
            />
          </div>
        </div>

        <div className="col-5 d-flex h-100 flex-row ps-2">
          <div className="d-flex flex-column justify-content-center align-items-start walton h-100">
            <p
              className="m-0"
              style={{ fontSize: "0.75rem" }}
            >
              {props.review.platformid}
            </p>
            <h6>{props.review.gametitle}</h6>
            <div className="d-flex flex-row justify-content-center align-items-start walton h-25">
              <img
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={clockIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.5rem" }}
              >
                1 month 30 days ago
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={chatIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                75
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={cannedIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                {props.review.likes}
              </p>
            </div>
          </div>
        </div>

        <div className="col-3 d-flex h-100 flex-row">
          <div className="col-8 d-flex flex-column justify-content-end align-items-end walton h-100">
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              {reviewer?.username}
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              {`lvl.${reviewer?.rank}`}
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              war pig
            </p>
          </div>
          <div className="col-4 d-flex flex-column justify-content-center align-items-end walton h-100 bg-danger">
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={reviewerIcon}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}


const ReviewCard = () => {

  return (
    <div className="col-12 text-white my-3">
      <div
        className="col-12 m-0 p-0 d-flex flex-row justify-content-center align-items-center rounded-2"
        style={{ height: "80px", backgroundColor: "#1E1E1E", border: "2px solid rgba(0, 255, 255, 0.2)" }}
      >

        <div className="col-4 d-flex h-100 flex-row">
          <div className="col-4 h-100  d-flex justify-content-center align-items-center">
            <div
              className="d-flex justify-content-center align-items-center bg-success"
              style={{ borderRadius: "60px", height: "50px", width: "50px" }}
            >
              <h4
                className="m-0 p-0"
                style={{ lineHeight: 0 }}
              >
                9.5
              </h4>
            </div>
          </div>
          <div className="col-8 d-flex justify-content-start align-items-center ms-auto"
            style={{ overflow: "hidden" }}>
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={anomalie01}
              alt=""
            />
          </div>
        </div>

        <div className="col-5 d-flex h-100 flex-row ps-2">
          <div className="d-flex flex-column justify-content-center align-items-start walton h-100">
            <p
              className="m-0"
              style={{ fontSize: "0.75rem" }}
            >
              pc:mod
            </p>
            <h6>stalker: anomaly</h6>
            <div className="d-flex flex-row justify-content-center align-items-start walton h-25">
              <img
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={clockIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.5rem" }}
              >
                1 month 30 days ago
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={chatIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                75
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={cannedIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                15
              </p>
            </div>
          </div>
        </div>

        <div className="col-3 d-flex h-100 flex-row">
          <div className="col-8 d-flex flex-column justify-content-end align-items-end walton h-100">
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              Harald
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              lvl.1
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              war pig
            </p>
          </div>
          <div className="col-4 d-flex flex-column justify-content-center align-items-end walton h-100 bg-danger">
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={reviewerIcon}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}


const NewReview = (props: { setModalActive: Dispatch<SetStateAction<Boolean>> }) => {
  return (
    <div
      className="col-12 text-white my-3"
      onClick={() => props.setModalActive(true)}
    >
      <div
        className="col-12 m-0 p-0 d-flex flex-row justify-content-center align-items-center  rounded-2 monomaniac"
        style={{ height: "80px", backgroundColor: "#1E1E1E", border: "2px solid rgba(0, 255, 255, 0.2)" }}
      >
        <h4 className="text-uppercase m-auto">+ add review</h4>
      </div>
    </div>
  )
}


const ReviewPage = () => {

  const user = useContext(UserContext);
  const [reviews, setReviews] = useState<IReview[]>([]);
  const [modalActive, setModalActive] = useState<Boolean>(false);


  useEffect(() => {
    // declare the async data fetching function
    const fetchData = async () => {
      // get the data from the api
      const data = await AxiosCall({ method: "GET", path: "review", data: "" });

      // set state with the result
      setReviews(data.rows);
    }

    // call the function
    fetchData()
      // make sure to catch any error
      .catch(console.error);
  }, [])


  const ReviewCall = async (props: IReview) => {
    if (CheckReview(props)) {
      if (await AxiosCall({ method: "POST", path: "review", data: props })) {
        window.alert("Review Added");
        setModalActive(false);
      }
      else {
        window.alert("Submit failed");
      }
    }
    else {
      window.alert("Submit failed !\n\nMake sure default values have been changed\nand there is no empty fields");
    }
  }


  const CheckReview = (props: IReview) => {
    if (props.userid === null ||
      props.score === null ||
      props.publishdate === "" ||
      props.imagepath === "" ||
      props.gametitle === "" ||
      props.review === "" ||
      props.platformid === 666 ||
      props.genreid === 666 ||
      props.developer === "" ||
      props.publisher === ""
    )
      return false;

    return true;
  }


  return (
    <>
      {modalActive && user.userData?.id &&
        <Review ReviewCall={ReviewCall} setModalActive={setModalActive} userId={user.userData?.id} />
      }
      <div className="col-12 d-flex justify-content-start align-items-center my-3 text-white">
        <h4>Reviews</h4>
      </div>
      <div
        className="row bg-dark rounded-4"
      >
        <div className="col-8">
          {user.userData?.logged && user.userData.id !== null &&
            <NewReview setModalActive={setModalActive} />
          }
          {
            reviews.map((review, key) => {
              return (
                <ReviewCardAlt key={key} review={review} />
              )
            })
          }
          <ReviewCard />
          <ReviewCard />
        </div>
        <div className="col-4 text-white">

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Platforms</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Genres</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Release Date</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <h5
            className="text-uppercase monomaniac mx-auto my-5 border-bottom"
            style={{ width: "60%" }}
          >
            show advanced
          </h5>

          <div
            className="d-flex flex-row mx-auto my-3"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Minimum score:</label>
            <input type="range" min="0" max="10" step="0.5" className="form-range w-50 ms-auto" id="customRange1"></input>
          </div>
          <div
            className="d-flex flex-row mx-auto my-2"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Developer:</label>
            <input type="text" className="input form-control-sm ms-auto w-50" />
          </div>
          <div
            className="d-flex flex-row mx-auto my-2"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Publisher:</label>
            <input type="text" className="input form-control-sm ms-auto w-50" />
          </div>
        </div>
      </div>
    </>
  );
}

export default ReviewPage;
