import image01 from "../assets/images/tarkov.jpg"
import image02 from "../assets/images/cyberpunk.jpg"


export const Videos = () => {

  return (

    <div className="row bg-dark rounded-2 my-5">
      <div className="col-3 ps-0 shadow-lg">
        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
          <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={image01} className="d-block w-100 p-2 rounded-4" style={{height: "200px"}} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{height: "430px"}} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{height: "430px"}} alt="..." />
            </div>
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
        </div>
      </div>
      <div className="col-3 pe-0 shadow-lg">
        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
          <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={image02} className="d-block d-block w-100 p-2 rounded-4" style={{ height: "200px" }} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{ height: "430px" }} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{ height: "430px" }} alt="..." />
            </div>
          </div>
        </div>
      </div>
      <div className="col-3 ps-0 shadow-lg">
        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
          <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={image01} className="d-block w-100 p-2 rounded-4" style={{height: "200px"}} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{height: "430px"}} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{height: "430px"}} alt="..." />
            </div>
          </div>
        </div>
      </div>
      <div className="col-3 pe-0 shadow-lg">
        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="true">
          <div className="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={image02} className="d-block d-block w-100 p-2 rounded-4" style={{ height: "200px" }} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{ height: "430px" }} alt="..." />
            </div>
            <div className="carousel-item">
              <img src="..." className="d-block" style={{ height: "430px" }} alt="..." />
            </div>
          </div>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </div>

  )
}

export default Videos;