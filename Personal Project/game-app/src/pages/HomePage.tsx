import News from "../components/News";
import Videos from "../components/Videos";
import Reviews from "../components/Reviews";

export const HomePage = () => {
  return (
    <>
      <div className='text-uppercase text-white monomaniac my-4 text-start'>
        <h5>space big news</h5>
      </div>
      <News />
      <Videos />
      <Reviews />
    </>
  );
};

export default HomePage;