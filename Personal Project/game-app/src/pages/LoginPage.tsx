import { useState } from "react";
import AxiosCall from "../api/APIHandler";
import { useContext } from 'react';
import { UserContext } from '../components/Context';
import { useNavigate } from "react-router-dom";
import { Login, ILogin } from "../components/Login";
import { Register, IRegister } from "../components/Register";

export const LoginPage = () => {

  const [showRegister, setShowRegister] = useState<Boolean>(false);

  const user = useContext(UserContext);
  const navigate = useNavigate();

  const LoginCall = async (props: ILogin) => {
    const data = await AxiosCall({ method: "POST", path: "getUser", data: props })
    if (data.rowCount > 0) {
      user.setUserData({
        logged: true,
        id: data.rows[0].id,
        username: data.rows[0].username,
        rank: data.rows[0].rank
      })
      window.alert("Logged in");
      navigate("/");
    }
    else {
      window.alert("Login failed");
    }
  }

  const RegisterCall = async (props: IRegister) => {
    if (RegisterCheck(props)) {
      if (await AxiosCall({ method: "POST", path: "user", data: props })) {
        window.alert("User Registered");
        setShowRegister(false);
      }
      else {
        window.alert("Register failed");
      }
    }
    else {
      window.alert("Register failed !\n\nMake sure default values have been changed\nand there is no empty fields");
    }
  }

  const RegisterCheck = (props: IRegister) => {
    if (props.username === "" ||
      props.password === "" ||
      props.fullname === "" ||
      props.country === "" ||
      props.role === 666)
      return false;

    return true;
  }

  return (
    <div
      className="row justify-content-center align-items-center"
    >
      <div
        className="position-relative col-8 h-100 d-flex flex-column justify-content-top align-items-center bg-dark rounded-4 shadow-lg"
        style={{ height: "600px" }}
      >
        {!showRegister &&
          <Login LoginCall={LoginCall} setShowRegister={setShowRegister} />
        }
        {showRegister &&
          <Register RegisterCall={RegisterCall} setShowRegister={setShowRegister} />
        }
      </div>
    </div >
  );
};

export default LoginPage;