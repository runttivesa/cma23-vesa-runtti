import reviewerIcon from "../assets/images/Reviewer_Icon.jpeg"
import clockIcon from "../assets/images/Clock_Icon.png"
import chatIcon from "../assets/images/Chat_Icon.png"
import cannedIcon from "../assets/images/Canned_Icon.png"
import anomalie01 from "../assets/images/Anomalie_Image_01.png"


const ReviewCard = () => {
  return (
    <div className="col-12 text-white my-3">
      <div
        className="col-12 m-0 p-0 d-flex flex-row justify-conent-center rounded-2 align-items-center"
        style={{ height: "80px", backgroundColor: "#1E1E1E", border: "2px solid rgba(0, 255, 255, 0.2)" }}
      >

        <div className="col-4 d-flex h-100 flex-row">
          <div className="col-4 h-100  d-flex justify-content-center align-items-center">
            <div
              className="d-flex justify-content-center align-items-center bg-success"
              style={{ borderRadius: "60px", height: "50px", width: "50px" }}
            >
              <h4
                className="m-0 p-0"
                style={{ lineHeight: 0 }}
              >
                9.5
              </h4>
            </div>
          </div>
          <div className="col-8 d-flex justify-content-start align-items-center ms-auto"
            style={{ overflow: "hidden" }}>
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={anomalie01}
              alt=""
            />
          </div>
        </div>

        <div className="col-5 d-flex h-100 flex-row ps-2">
          <div className="d-flex flex-column justify-content-center align-items-start walton h-100">
            <p
              className="m-0"
              style={{ fontSize: "0.75rem" }}
            >
              pc:mod
            </p>
            <h6>stalker: anomaly</h6>
            <div className="d-flex flex-row justify-content-center align-items-start walton h-25">
              <img
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={clockIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.5rem" }}
              >
                1 month 30 days ago
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={chatIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                75
              </p>
              <img
                className="ms-2"
                style={{ objectFit: "cover", height: "100%", width: "100%" }}
                src={cannedIcon}
                alt=""
              />
              <p
                className="text-nowrap my-auto ms-2"
                style={{ fontSize: "0.75rem" }}
              >
                15
              </p>
            </div>
          </div>
        </div>

        <div className="col-3 d-flex h-100 flex-row">
          <div className="col-8 d-flex flex-column justify-content-end align-items-end walton h-100">
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              Harald
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              lvl.1
            </p>
            <p
              className="my-0 me-3 mb-1"
              style={{ fontSize: "0.5rem" }}
            >
              war pig
            </p>
          </div>
          <div className="col-4 d-flex flex-column justify-content-center align-items-end walton h-100 bg-danger">
            <img
              style={{ objectFit: "cover", height: "100%", width: "100%" }}
              src={reviewerIcon}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}


const ReviewPage = () => {
  return (
    <>
      <div className="col-12 d-flex justify-content-start align-items-center my-3 text-white">
        <h4>Reviews</h4>
      </div>
      <div
        className="row bg-dark rounded-4"
      >
        <div className="col-8">
          <ReviewCard />
          <ReviewCard />
        </div>
        <div className="col-4 text-white">

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Platforms</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Genres</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <select
            className="form-select form-select-sm my-3 mx-auto maretha"
            aria-label=".form-select example"
            style={{ width: "60%" }}
          >
            <option selected>Release Date</option>
            <option value="1">pc</option>
            <option value="2">xbox</option>
            <option value="3">playstation</option>
          </select>

          <h5
            className="text-uppercase monomaniac mx-auto my-5 border-bottom"
            style={{ width: "60%" }}
          >
            show advanced
          </h5>

          <div
            className="d-flex flex-row mx-auto my-3"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Minimum score:</label>
            <input type="range" min="0" max="10" step="0.5" className="form-range w-50 ms-auto" id="customRange1"></input>
          </div>
          <div
            className="d-flex flex-row mx-auto my-2"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Developer:</label>
            <input type="text" className="input form-control-sm ms-auto w-50" />
          </div>
          <div
            className="d-flex flex-row mx-auto my-2"
            style={{ width: "60%" }}
          >
            <label className="maretha my-auto text-nowrap">Publisher:</label>
            <input type="text" className="input form-control-sm ms-auto w-50" />
          </div>
        </div>
      </div>
    </>
  );
}

export default ReviewPage;
