import pg from "pg";
const { Pool } = pg;

const pool = new Pool({
    user: "postgres",
    password: "postgres",
    host: "localhost",
    port: 5433,
    database: "gameapp"
});

export default pool;