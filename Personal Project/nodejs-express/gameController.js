import pool from "./db.js";

const getAllgames = async (req, res) => {

  try {
    const allGames = await pool.query("SELECT * FROM games");
    res.json(allGames.rows);
    console.log(allGames);

  } catch (err) {
    console.error(err.message)
  }
}

const getOneGame = async (req, res) => {
  try {
    const { id } = req.params;
    const oneGame = await pool.query("SELECT * FROM games WHERE id = $1", [id]);
    res.json(oneGame.rows[0]);

  } catch (err) {
    console.error(err.message)
  }
}

const updateOneGame = async (req, res) => {
  try {

    const { id } = req.params;
    const { title } = req.body

    const updatedGame = await pool.query("UPDATE games SET title = $1 WHERE id= $2", [title, id])

    res.json("Game has been updated")
  } catch (err) {
    console.error(err.message)

  }
};


const deleteOneGame = async (req, res) => {
  try {
    const { id } = req.params;
    const deleteGame = await pool.query("DELETE FROM games WHERE id = $1", [id]);
    res.json("Game has been deleted");
  } catch (err) {
    console.error(err.message)

  }
}

export default {
  getAllgames,
  getOneGame,
  updateOneGame,
  deleteOneGame
}

