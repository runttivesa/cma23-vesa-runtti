import pool from "./db.js";

const createReview = async (req, res) => {
  try {
    const {
      userid,
      score,
      publishdate,
      createddate,
      modifieddate,
      imagepath,
      likes,
      dislikes,
      gametitle,
      review,
      platformid,
      genreid,
      developer,
      publisher
    } = req.body;
    const newReview = await pool.query(`INSERT INTO reviews
        (userid, score, publishdate, createddate, modifieddate, imagepath, likes, dislikes, gametitle, review, platformid, genreid, developer, publisher)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) RETURNING *`,
      [userid, score, publishdate, createddate, modifieddate, imagepath, likes, dislikes, gametitle, review, platformid, genreid, developer, publisher]);
    res.json(newReview);
  } catch (err) {
    console.error(err.message)
  }
}

const getReviews = async (req, res) => {
  try {
    const reviews = await pool.query(`SELECT * FROM reviews`);
    res.json(reviews);
  } catch (err) {
    console.error(err.message)
  }
}

export default {
  createReview,
  getReviews
}

