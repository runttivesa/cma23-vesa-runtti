import express from "express";
import cors from 'cors';
import gameController from "./gameController.js";
import userController from "./userController.js";
import reviewController from "./reviewController.js";
const server = express();
const port = 8080;

/* Allow access from local port 3000 to 8080 */
const options = {
    origin: ['http://localhost:3000']
};

/* Parser */
server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(cors(options));

/* User Requests */
server.post("/user", userController.createUser);
server.post("/getUser", userController.getUser);
server.get("/getUser/:id", userController.getUserProfile);

/* Review Requests */
server.post("/review", reviewController.createReview);
server.get("/review", reviewController.getReviews);

/* Game Requests */
server.get("/games", gameController.getAllgames);
server.get("/games/:id", gameController.getOneGame);
server.put("/games/:id", gameController.updateOneGame);
server.delete("/games/:id", gameController.deleteOneGame);

server.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})