import pool from "./db.js";

const createUser = async (req, res) => {
  try {
    const {
      role,
      rank,
      imagePath,
      username,
      password,
      fullname,
      birthdate,
      country,
      createdDate,
      modifiedDate
    } = req.body;
    const newGame = await pool.query(`INSERT INTO users
    (role, rank, imagePath, username, password, fullname, birthdate, country, createdDate, modifiedDate)
    VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *`, [role, rank, imagePath, username, password, fullname, birthdate, country, createdDate, modifiedDate]);
    res.json(newGame);
  } catch (err) {
    console.error(err.message)
  }
}

const getUser = async (req, res) => {
  console.log(req.body);
  try {
    const {
      username,
      password
    } = req.body;
    const user = await pool.query(`SELECT id, username, rank FROM users
    WHERE username = $1 AND password = $2`, [username, password]);
    console.log(res.json(user))
    res.json(user);
  } catch (err) {
    console.error(err.message)
  }
}

const getUserProfile = async (req, res) => {
  try {
    const { id } = req.params;
    const userProfile = await pool.query("SELECT username, rank FROM users WHERE id = $1", [id]);
    res.json(userProfile);

  } catch (err) {
    console.error(err.message)
  }
}

export default {
  createUser,
  getUser,
  getUserProfile
}