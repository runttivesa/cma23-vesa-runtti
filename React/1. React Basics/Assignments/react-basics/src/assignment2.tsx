import './App.css';

export const Assigment2 = () => {

  return (
    <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center", alignItems: "center", height: "400px", width: "400px", padding: "10px", backgroundColor: "navy" }}>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center", marginTop: "1rem"  }}>
        <img
          style={{ height: "auto", width: "90%", objectFit: "contain" }}
          src="https://gitea.buutti.com/education/academy-assignments/raw/branch/master/React/1.%20React%20Basics/r2d2.jpg"
          alt=""
        />
      </div>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        <h2 style={{ color: "white" }}>Hello, I am R2D2</h2>
      </div>
      <div style={{ display: "flex", justifyContent: "center", alignItems: "center", marginBottom: "3rem" }}>
        <p style={{ color: "white" }}>
          BeeYoop BeeDeepBoom Weeop DEEpaEEya !
        </p>
      </div>
    </div>
  );
}
