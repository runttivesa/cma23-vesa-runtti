import './App.css';


export const Assigment3 = () => {
  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic", }
  ];

  const Planets = () => {
    return (
      <table>
        {planetList.map((planet) => {
          return (
            <tr>
              <th>{planet.name}</th>
              <th>{planet.climate}</th>
            </tr>
          )
        })
        }
      </table>
    )
  }



  return (
      <Planets/>
  );
}