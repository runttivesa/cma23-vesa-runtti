import React, { useState } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export const Assigment4 = () => {
  interface Book {
    name?: string;
    pages?: number;
  }

  const [books, setBooks] = useState<Book[]>([
    {
      name: "Dune",
      pages: 412
    },
    {
      name: "The Eye of the World",
      pages: 782
    }
  ])
  
  const [name, setName] = useState<string>()
  const [pages, setPages] = useState<number>()

  const Books = () => {
    return (
      <ul className="list-group mb-3">
        {books.map((book) => {
          return (
            <li className="list-group-item d-flex justify-content-between lh-sm">
              <div>
                <p className="my-0">{book.name}</p>
                <small className="text-body-secondary">{book.pages}</small>
              </div>
            </li>
          )
        })
        }
      </ul>
    )
  }

  return (
    <body className="bg-body-tertiary">
      <div className="container">
        <main>
          <div className="row g-5">
            <h4 className="d-flex justify-content-between align-items-center mb-3">
              Book List Service
            </h4>
            <div className="col-md-4">
              <h6 className="d-flex justify-content-between align-items-center mb-3">
                Books:
              </h6>
              <Books />
              <div className="col-md-8 my-5">
                <label htmlFor="bookName">Add new book:</label>
                <input className="form-control my-3" type="text" id="bookName" placeholder="Book name" onChange={(event => setName(event.target.value))} />
                <input className="form-control my-3" type="text" id="pageCount" placeholder="Page count" onChange={(event => setPages(Number(event.target.value)))} />
                <button type="button" className="btn btn-primary btn-sm" onClick={() => setBooks((prevState) => [...prevState, { name, pages }])}>Add book</button>
              </div>
            </div>
          </div>
        </main>
      </div >
    </body >
  );
}