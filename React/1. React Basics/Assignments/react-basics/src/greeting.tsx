import React from 'react';

export const Greeting = (props: { name: String, age: number }) => {
  return (
    <p> "My name is {props.name} and I am age years {props.age}!"</p>
  );
}