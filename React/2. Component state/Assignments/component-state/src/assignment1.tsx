import React, { useState } from "react";
import './App.css';


export const Assigment1 = () => {

  const [showText, setShowText] = useState<boolean>(true)

  return (
    <div>
      <h2>Assingment 1</h2>
      <br />
      <button onClick={() => setShowText(!showText)}>Toggle Text</button>
      <br />
      {showText &&
        <p>Fear is the path to the dark side.</p>
      }
    </div>
  );
}
