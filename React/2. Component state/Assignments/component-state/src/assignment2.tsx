import React, { useState, useEffect } from "react";
import './App.css';

const Buttons = () => {

  const [values, setValues] = useState<boolean[]>([])

  useEffect(() => {
    for (let i = 1; i <= 5; i++) {
      setValues((prevState) => [...prevState, true]);
    }
  }, [])


  const handleClick = (value: number) => {
    const checkedPos = [...values];
    checkedPos[value] = false;
    setValues(checkedPos);
    console.log(values);
  }

  return (
    <div  style={{ display: "flex", width: "100vw", justifyContent: "center", alignItems: "center" }}> 
      {
        values.map((element, key) => {
          console.log(key)
          return (
            <div key={key}>
              {
                element &&
                <button onClick={() => handleClick(key)}>Button {key + 1}</button>
              }
            </div>
          )
        })
      }
    </div>
  );
};

export const Assigment2 = () => {

  return (
    <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center", alignItems: "center" }}>
      <div  style={{ display: "flex", width: "100vw", justifyContent: "center", alignItems: "center" }}>
        <h2>Assignment 2</h2>
      </div>
      <Buttons />
    </div>
  );
}
