import React, { useState, useEffect } from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export const Assigment3 = () => {

  const names = [
    "Anakin Skywalker",
    "Leia Organa",
    "Han Solo",
    "C-3PO",
    "R2-D2",
    "Darth Vader",
    "Obi-Wan Kenobi",
    "Yoda",
    "Palpatine",
    "Boba Fett",
    "Lando Calrissian",
    "Jabba the Hutt",
    "Mace Windu",
    "Padmé Amidala",
    "Count Dooku",
    "Qui-Gon Jinn",
    "Aayla Secura",
    "Ahsoka Tano",
    "Ki-Adi-Mundi",
    "Luminara Unduli",
    "Plo Koon",
    "Kit Fisto",
    "Shmi Skywalker",
    "Beru Whitesun",
    "Owen Lars"
  ];

  const [values, setValues] = useState<boolean[]>([])


  useEffect(() => {
    for (let i = 1; i <= names.length; i++) {
      setValues((prevState) => [...prevState, false]);
    }
  }, [names.length])

  const handleClick = (value: number) => {
    const checkedPos = [...values];
    checkedPos[value] = !checkedPos[value];
    setValues(checkedPos);
  }

  const Grid = () => {
    return (
      <div className="d-flex col-md-6 row justify-items-center align-items-center">
        {
          names.map((element, key) => {
            return (
              <div
                className="d-flex justify-content-center align-items-center"
                style={{ width: "calc(100% / 5)", height: "160px", border: "1px solid black", backgroundColor: values[key] ? 'green' : 'red' }}
                onClick={() => handleClick(key)}
                key={element}
              >
                <p>{element}</p>
              </div>
            )
          })
        }
      </div>
    );
  }


  return (
    <>
      <Grid />
    </>
  );
}