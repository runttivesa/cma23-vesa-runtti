import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, Link } from "react-router-dom";
import { Assigment1 } from "./assignment1";
import { Assigment2 } from "./assignment2";
import { Assigment3 } from "./assignment3";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div className="App">
        <header className="App-header">
          <Link to="/assignment1">Assignment 1</Link>
          <Link to="/assignment2">Assignment 2</Link>
          <Link to="/assignment3">Assignment 3</Link>
        </header>
      </div>
    ),
  },
  {
    path: "/assignment1",
    element: <Assigment1 />,
  },
  {
    path: "/assignment2",
    element: <Assigment2 />,
  },
  {
    path: "/assignment3",
    element: <Assigment3 />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <RouterProvider router={router} />
);