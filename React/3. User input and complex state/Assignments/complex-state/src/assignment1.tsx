import { TaskList } from "./components/TaskList";
import './App.css';

export const Assigment1 = () => {

  return (
    <main className="main vw-100">
      <div className="container position-relative">
        <TaskList />
      </div>
    </main >
  );
}