import { useState } from "react";


const CounterButtonsArray = () => {

  const [count, setCount] = useState<Array<number>>([0, 0, 0, 0])


  const Increment = (key: number) => {
    if (key < 3) {
      let newArray = [...count]
      newArray[key] += 1;
      setCount(newArray);
    }

    return;
  }

  return (
    <div>
      {
        count.map((element, key) => {
          return (
            <div className="d-flex col-md-1 justify-content-center align-content-center">
              {key < 3 &&
                < button className="btn btn-secondary my-2" key={`${element}#${key}`} onClick={() => Increment(key)}>{element}</ button>
              }
              {key >= 3 &&
                <h5 className="text-white">{count[0] + count[1] + count[2]}</h5>
              }
            </div>
          )
        })
      }
    </div >
  );
};

export default CounterButtonsArray;