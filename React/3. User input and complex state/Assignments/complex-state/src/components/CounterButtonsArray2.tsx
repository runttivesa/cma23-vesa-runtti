import { useState } from "react";

interface Counter {
  id: number,
  value: number
}

const CounterButton = (props: { calculateSum: Function }) => {

  const [count, setCount] = useState<Counter>(
    { id: 0, value: 0 },
  )

  const Increment = () => {
    setCount({
      ...count, value: count.value + 1
    });
  }

  return (
    <>
      < button className="btn btn-secondary my-2" onClick={() => { Increment(); props.calculateSum() }}>{count.value}</ button>
    </>
  );
};


const CounterButtonsArray = (props: { lenght: number }) => {

  const data = Array.from({ length: props.lenght }, (_, index) => index);
  const [sum, setSum] = useState<number>(0);

  const calculateSum = () => {
    setSum(sum + 1);
  }

  return (
    <>
      {data.map((element, key) =>
        <div key={`${element}#${key}`}>
          <CounterButton calculateSum={calculateSum} />
        </div>
      )}
      <h5 className="text-white">{sum}</h5>
    </>
  );
};

export default CounterButtonsArray;