import { forwardRef, useImperativeHandle, useState } from "react";

export interface RefType {
  Increment: (field: string, value: number) => void;
}

export interface Counter {
  field: string,
  value: number
}

const ObjectState = forwardRef((props: { field: string, value: number }, ref) => {
  useImperativeHandle(ref, () => ({ Increment }))

  const [count, setCount] = useState<Counter>({ field: "", value: 0 });

  const Increment = () => {
    setCount(({ field: props.field, value: props.value + count.value }));
  }

  return (
    <div>
      <button className="btn btn-secondary my-2">{count.value}</ button>
      <h3 className="text-white">Text: {count.field}</h3>
      <h4 className="text-white">Iteration: {count.value}</h4>
    </div>
  );
});

export default ObjectState;