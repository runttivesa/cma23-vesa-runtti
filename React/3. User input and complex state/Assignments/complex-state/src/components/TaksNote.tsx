import { useState, useEffect } from "react";
import { Task } from "../data/data";
import '../App.css';

export const TaskNote = (props:
  {
    id: number,
    task: string,
    complete: boolean,
    date: string,
    subTasks: Task[],
    toggleMainTask: Function,
    toggleSubTask: Function,
    removeTask: Function,
    setNewTaskName: Function
  }
) => {

  const [editMode, setEditMode] = useState<boolean>(false)
  const [task, setTask] = useState<string>("")


  useEffect(() => {
    setTask(props.task);
  }, [props.task])

  const enableEditMode = () => {
    if (editMode) {
      props.setNewTaskName(task, props.id)
    }

    setEditMode(!editMode);
  }

  const cancelEditMode = () => {
    setTask(props.task);
    setEditMode(false);
  }


  return (
    <div className="container py-5 h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col col-lg-8 col-xl-6">
          <div className="card rounded-3 shadow-sm">
            <div className="card-body p-4">
              <div className="d-flex col-md-12 row justify-content-start align-items-center">
                <input
                  className="taskCheckbox form-check-input me-3"
                  type="checkbox"
                  checked={props.complete}
                  aria-label="..."
                  onChange={() => props.toggleMainTask(props.id)}
                />
                <input className="col-md-8 h2 bg-transparent"
                  style={{ border: !editMode ? "1px solid transparent" : "1px solid grey", borderRadius: "0.2rem" }}
                  disabled={!editMode} type="text"
                  value={task}
                  onChange={(event) => setTask(event.target.value)}
                />
                {!props.complete &&
                  <span className="badge col-md-2 bg-danger">NOT READY</span>
                }
                {props.complete &&
                  <span className="badge col-md-2 bg-success">READY</span>
                }
                <button className="btn col-md-1 btn-primary ms-auto" style={{ width: "7%" }} onClick={() => props.removeTask(props.id)}>X</button>
              </div>
              <p className="text-muted pb-2">{props.date} • {props.id}</p>
              <ul className="list-group rounded-0">
                {
                  props.subTasks.map((element, key) => {
                    return (
                      <li
                        key={`${props.task}#${props.id}#${element.task}#${element.id}`}
                        className="list-group-item border-0 d-flex align-items-center ps-0"
                      >
                        <input
                          className="form-check-input me-3"
                          type="checkbox"
                          checked={element.complete}
                          aria-label="..."
                          onChange={() => props.toggleSubTask(element.id, props.id)}
                        />
                        {
                          element.complete &&
                          <p className="me-auto my-auto text-center"><s>{element.task}</s></p>
                        }
                        {!element.complete &&
                          <p className="me-auto my-auto text-center">{element.task}</p>
                        }
                      </li>
                    )
                  })
                }
              </ul>
              <button
                className="btn p-0 m-auto mt-5"
                style={{ height: "30px", width: "60px", lineHeight: "0", background: !editMode ? "blue" : "green", color: "whitesmoke" }}
                onClick={() => enableEditMode()}
              >
                {`${!editMode ? "EDIT" : "SAVE"}`}
              </button>
              {editMode &&
                <button
                  className="btn p-0 m-auto mt-5 bg-danger text-white ms-2"
                  style={{ height: "30px", width: "80px", lineHeight: "0" }}
                  onClick={() => cancelEditMode()}
                >CANCEL</button>
              }
            </div>
          </div>
        </div>
      </div>
    </div >
  );
}
