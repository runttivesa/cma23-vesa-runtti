import { useState, useEffect, useCallback } from "react";
import { defaultState, defaultTasks, Note } from "../data/data";
import { TaskNote } from "./TaksNote";
import '../App.css';

export const TaskList = () => {

  const [tasks, setTasks] = useState<Note[]>([])
  const [newTask, setNewTask] = useState<Note>();
  const [isBlocked, setIsBlocked] = useState<boolean>(true);
  const [showModal, setShowModal] = useState<boolean>(false);


  const checkForm = useCallback(() => {
    if ((newTask?.task === "") ||
      (newTask?.subTasks && newTask.subTasks.length > 0 && newTask?.subTasks.find((subTask) => subTask.task === "")) ||
      (newTask?.date === ""))
      setIsBlocked(true);
    else
      setIsBlocked(false);
  }, [newTask?.task, newTask?.date, newTask?.subTasks]);


  useEffect(() => {
    if (tasks === undefined || tasks === null || tasks.length === 0)
      setTasks(defaultTasks);
    if (showModal)
      checkForm();

  }, [tasks, showModal, checkForm])


  const removeTask = (id: number) => {
    let newTasks = [...tasks];

    newTasks.forEach((item, index) => {
      if (item.id === id) {
        newTasks.splice(index, 1);
      }
    });

    setTasks(newTasks);
  }


  const toggleMainTask = (id: number) => {
    let newTasks = [...tasks];
    let bool: boolean = true;

    for (let task of newTasks)
      if (task.id === id)
        if (task.subTasks.length > 0)
          for (let subTask of task.subTasks)
            if (!subTask.complete)
              bool = false

    if (bool) {
      for (let task of newTasks) if (task.id === id)
        task.complete = !task.complete;

      setTasks(newTasks);
      return;
    }

    alert("Please check all the subtask \nbefore trying to check main task !")
    return;
  }


  const toggleSubTask = (id: number, mainID: number) => {

    let newTasks = [...tasks];

    for (let item of newTasks)
      if (item.id === mainID) {
        if (!item.subTasks[id].complete === false) {
          item.complete = false;
        }
        item.subTasks[id].complete = !item.subTasks[id].complete;
      }

    setTasks([...newTasks]);
  };


  const setNewTaskName = (value: string, id: number) => {
    let newTasks = [...tasks];

    for (let task of newTasks) if (task.id === id)
      task.task = value;

    setTasks(newTasks);
  }

  return (
    <>
      <div className="container">
        {
          tasks.map((element, key) => {
            return (
              <TaskNote
                key={`${element.task}#${key}`}
                id={tasks[key].id}
                task={tasks[key].task}
                date={tasks[key].date}
                complete={tasks[key].complete}
                subTasks={tasks[key].subTasks}
                removeTask={removeTask}
                toggleMainTask={toggleMainTask}
                toggleSubTask={toggleSubTask}
                setNewTaskName={setNewTaskName}
              />
            )
          })
        }
      </div>
      <div className="addButton d-flex justify-content-center align-items-center position-fixed">
        <h3 className="mb-5 text-white">Add</h3>
        <button
          className="btn btn-primary position-fixed mt-5"
          onClick={() => {
            setShowModal(true);
            setNewTask(defaultState);
          }}
        >+</button>
      </div>
      {showModal && newTask &&
        <div
          className="drawer"
          id="modal"
          onClick={(event) => { if ((event.target as HTMLDivElement).id === "modal") setShowModal(false) }}
        >
          <div className="taskNote d-flex col-md-12 rounded shadow-lg justify-content-center align-content-center mx-auto mb-auto"
          >
            <form className="col-md-8 was-validated">
              <div className="my-4">
                <label htmlFor="validationTask" className="form-label">Task Name</label>
                <input
                  type="text"
                  className="form-control is-invalid"
                  id="validationTask"
                  placeholder="Required task name"
                  required
                  value={newTask.task}
                  onChange={(event) =>
                    setNewTask({ ...newTask, task: event?.target.value })
                  } />
              </div>
              <input
                className="form-control"
                type="date"
                name="datePicker"
                placeholder="DateRange"
                value={newTask.date}
                onChange={(event) =>
                  setNewTask({ ...newTask, date: event?.target.value })
                }
              />
              <div className="d-flex row justify-content-around align-content-center mt-5 mb-4">
                <h4 className="mb-3 text-center">Sub Tasks</h4>
                <button
                  className="taskButton btn btn-primary m-0 p-0"
                  type="button"
                  onClick={() => {
                    setNewTask({ ...newTask, subTasks: [...newTask.subTasks, { id: newTask.subTasks.length, task: "", complete: false }] });
                  }}>+</button>
                <button
                  className="taskButton btn btn-primary m-0 p-0"
                  type="button"
                  onClick={() => {
                    const items = [...newTask.subTasks].filter((item, i) => i !== newTask.subTasks.length - 1);
                    setNewTask({ ...newTask, subTasks: items });
                  }}>-</button>
              </div>
              {
                newTask.subTasks.map((element, key) => {
                  return (
                    <div
                      key={key}
                      className="d-flex  mb-1"
                    >
                      <label htmlFor={`validationSubTask${key}`} className="form-label">{key}</label>
                      <input
                        type="text"
                        className="form-control is-invalid ms-3"
                        id={`validationSubTask${key}`}
                        value={element.task}
                        onChange={(event) => {
                          const items = [...newTask.subTasks];
                          items[key].task = event.target.value;
                          setNewTask({ ...newTask, subTasks: items });
                        }}
                        required
                      />
                    </div>
                  )
                })
              }

              <div className="d-flex justify-content-center align-items-center mt-5 mb-4">
                <button className="btn btn-primary" type="button" disabled={isBlocked} onClick={() => {
                  newTask.id = tasks.length + 1;
                  const newTasks = [...tasks]
                  newTasks.push(newTask);
                  newTasks.sort(function compare(a, b) {
                    return Number(new Date(b.date)) - Number(new Date(a.date));
                  });
                  setTasks(newTasks);
                  setNewTask(defaultState);
                }}>Add Task</button>
              </div>
            </form>
          </div >
        </div >
      }
    </>
  );
};