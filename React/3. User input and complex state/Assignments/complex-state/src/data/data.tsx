export const defaultTasks = [
    { id: 1, task: 'Buy potatoes', date: "2023-10-19", complete: false, subTasks: [] },
    { id: 2, task: 'Make food', date: "2023-10-19", complete: false, subTasks: [] },
    { id: 3, task: 'Exercise', date: "2023-10-19", complete: false, subTasks: [] },
    { id: 4, task: 'Do the dishes', date: "2023-10-19", complete: false, subTasks: [] },
    { id: 5, task: 'Floss the teeth', date: "2023-10-19", complete: false, subTasks: [] },
    { id: 6, task: 'Play videogames', date: "2023-10-19", complete: true, subTasks: [] },
    { id: 7, task: 'Buy Cake', date: "2023-10-19", complete: false, subTasks: [{ id: 0, task: "Eat Cake", complete: true }, { id: 1, task: "Do Not Eat Cake", complete: true }] },
]

export const defaultState = {
    id: 0,
    task: '',
    date: new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate(),
    complete: false,
    subTasks: []
};

export interface Task {
    id: number,
    task: string,
    complete: boolean
}

export interface Note extends Task {
    subTasks: Task[],
    date: string
}