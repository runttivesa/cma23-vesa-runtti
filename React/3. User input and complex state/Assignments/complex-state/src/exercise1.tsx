import React, { useState } from "react";


export const Exercise1 = () => {

  const [inputValue, setInputValue] = useState<string>()

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>
      <div className="container">
        <div className="d-flex col-md-12 flex-column position-relative justify-content-center align-items-center">
          <h4 className="text-white">{inputValue}</h4>
          <input type="text" onChange={(event) => setInputValue(event.target.value)} />
        </div>
      </div>
    </div >
  );
}