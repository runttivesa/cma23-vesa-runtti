import React, { useState } from "react";


export const Exercise2 = () => {

  const [inputValue, setInputValue] = useState<string>()
  const [submitValue, setSubmitValue] = useState<string>()

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>

      <div className="container">
        <div className="d-flex col-md-12 flex-column position-relative justify-content-center align-items-center">
          <h4 className="text-white">Your String is: {submitValue}</h4>
          <input type="text" value={inputValue} onChange={(event) => setInputValue(event.target.value)} />
          <br />
          <button className="btn btn-primary" onClick={() => setSubmitValue(inputValue)}>Submit</button>
        </div>
      </div>

    </div >
  );
}