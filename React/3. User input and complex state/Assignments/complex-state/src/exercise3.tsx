import React, { useState } from "react";


export const Exercise3 = () => {

  const [inputValue, setInputValue] = useState<string>()
  const [submitValue, setSubmitValue] = useState<string>()

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    setSubmitValue(inputValue);
  }

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>

      <div className="container">
        <div className="d-flex col-md-12 flex-column position-relative justify-content-center align-items-center">
          <form className="d-block text-center" onSubmit={(event) => handleSubmit(event)}>
            <h4 className="text-white">Your String is: {submitValue}</h4>
            <input type="text" value={inputValue} onChange={(event) => setInputValue(event.target.value)} />
            <br />
            <br />
            <button className="btn btn-primary" type="submit">Submit</button>
          </form>
        </div>
      </div>

    </div >
  );
}