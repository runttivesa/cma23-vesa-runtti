import React, { useState, useRef } from "react";
import ObjectState, { RefType } from "./components/ObjectState";

export const Exercise4 = () => {

  const objectStateRef = useRef<RefType>(null);

  const [fieldValue, setFieldValue] = useState<string>("");

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>
      <div className="container">
        <div className="d-flex col-md-12 flex-column position-relative justify-content-center align-items-center">
          <form className="d-block text-center" onSubmit={(event) => { event.preventDefault(); objectStateRef.current?.Increment(fieldValue, 1); }}>
            <ObjectState ref={objectStateRef} field={fieldValue} value={1} />
            <input type="string" value={fieldValue} onChange={(event) => setFieldValue(event.target.value)} />
            <br />
            <br />
            <button className="btn btn-primary" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div >
  );
}