import React from "react";
import CounterButtonsArray from "./components/CounterButtonsArray";

export const Exercise5 = () => {

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>
      <div className="container">
        <div className="d-flex col-md-12 flex-column flex-wrap position-relative justify-content-center align-items-center">
          <CounterButtonsArray />
        </div>
      </div>
    </div >
  );
}