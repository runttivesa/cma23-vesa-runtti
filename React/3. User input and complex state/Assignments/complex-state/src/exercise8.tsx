import React from "react";
import CounterButtonsArray2 from "./components/CounterButtonsArray2";

export const Exercise8 = () => {

  return (
    <div className="d-flex vw-100 vh-100 position-relative justify-content-center align-items-center" style={{ backgroundColor: "#3c506b" }}>
      <div className="container">
        <div className="d-flex col-md-12 flex-column flex-wrap position-relative justify-content-center align-items-center">
          <CounterButtonsArray2 lenght={3} />
        </div>
      </div>
    </div >
  );
}