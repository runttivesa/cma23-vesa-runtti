import * as React from "react";
import * as ReactDOM from "react-dom/client";
import 'bootstrap/dist/css/bootstrap.min.css';
import { createBrowserRouter, RouterProvider, Link } from "react-router-dom";
import { Assigment1 } from "./assignment1";
import { Exercise1 } from "./exercise1";
import { Exercise2 } from "./exercise2";
import { Exercise3 } from "./exercise3";
import { Exercise4 } from "./exercise4";
import { Exercise5 } from "./exercise5";
import { Exercise8 } from "./exercise8";


const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div className="App">
        <header className="App-header">
          <Link to="/assignment1">Assignment 1</Link>
          <Link to="/exercise1">Exercise 1</Link>
          <Link to="/exercise2">Exercise 2</Link>
          <Link to="/exercise3">Exercise 3</Link>
          <Link to="/exercise4">Exercise 4</Link>
          <Link to="/exercise5">Exercise 5</Link>
          <Link to="/exercise8">Exercise 8</Link>
        </header>
      </div>
    ),
  },
  {
    path: "/assignment1",
    element: <Assigment1 />,
  },
  {
    path: "/exercise1",
    element: <Exercise1 />,
  },
  {
    path: "/exercise2",
    element: <Exercise2 />,
  },
  {
    path: "/exercise3",
    element: <Exercise3 />,
  },
  {
    path: "/exercise4",
    element: <Exercise4 />,
  },
  {
    path: "/exercise5",
    element: <Exercise5 />,
  },
  {
    path: "/exercise8",
    element: <Exercise8 />,
  }
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <RouterProvider router={router} />
);