import { createBrowserRouter, RouterProvider, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import Home from "./components/Home";
import About from "./components/About";
import Links from "./components/Links";
import Notes from "./components/Notes";
import './App.css';

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <>
        <Link to="/home">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/links">Links</Link>
      </>
    ),
  },
  {
    path: "/home",
    element: <Home />,
  },
  {
    path: "/about",
    element: <About />,
  },
  {
    path: "/links",
    element: <Links />,
  },
]);

function App() {

  return (
    <div className="App">
      <header className="App-header">
       {/*  <RouterProvider router={router} /> */}
        <Notes />
      </header>
    </div>
  );
}

export default App;
