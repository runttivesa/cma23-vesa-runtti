import { INote } from "./data";

export const Note = (props: { note: INote, toggle: Function }) => {

  return (
    <div className="d-flex col-md-12 row justify-content-center align-items-center">
      <div className="col-md-1">
        <p>
          {props.note.id}
        </p>
      </div>
      <div className="d-flex col-md-4">
        <p>
          {props.note.content}
        </p>
      </div>
      <div className="d-flex col-md-4 me-auto">
        <p>
          {props.note.date.toString()}
        </p>
      </div>
      <div className="d-flex col-md-1 me-auto">
        <p>
          {props.note.important.toString()}
        </p>
      </div>
      <div className="d-flex col-md-2 me-auto">
        <button
          className="btn btn-primary"
          autoCapitalize="true"
          onClick={() => props.toggle(props.note.id)}
        >
          Toggle
        </button>
      </div>
    </div>
  );
};