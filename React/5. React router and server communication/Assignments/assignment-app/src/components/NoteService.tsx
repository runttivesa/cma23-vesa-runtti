import axios from 'axios'
import { INote } from './data';
const baseUrl = 'http://localhost:5000/notes/';

const getNote = async (parameter?: string) => {
  return await axios.get(baseUrl + (parameter ? parameter : ""));
}

const postNote = async (newObject: INote) => {
  return await axios.post(baseUrl, newObject);
}

const updateNote = async (newObject: INote) => {
  return await axios.put(`${baseUrl}/${newObject.id}`, newObject);
}

const deleteNote = async (parameter?: number) => {
  if (parameter)
    return await axios.delete(baseUrl + parameter);
}

const exportedObject = {
  getNote,
  postNote,
  updateNote,
  deleteNote
};

export default exportedObject;