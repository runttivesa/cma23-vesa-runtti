import { useEffect, useState } from "react";
import NoteService from "./NoteService";
import { Note } from "./Note";
import { INote } from "./data";

interface IButton {
  id: number,
  method: string,
  open: boolean
  useParam: boolean,
  parameters: INote
}

const Button = (props: { button: IButton, changeState: Function, request: Function }) => {

  return (
    <div className="col-md-2 mx-4 flex-column justify-content-center align-items-center">
      <span style={{ fontSize: "32px" }}>&#8637;</span>
      <button
        className="btn btn-primary"
        style={{
          background: "transparent",
          border: "none",
          outline: "none"
        }}

        autoCapitalize="true"
        onClick={() => props.changeState("open", props.button.id)}
      >
        {props.button.method}
      </button >
      <span style={{ fontSize: "32px" }}>&#8641;</span>
      <div
        className="col-md-12 position-relative justify-content-center align-items-center rounded-2 shadow"
        style={{
          height: props.button.open ? "300px" : "0px",
          backgroundColor: props.button.open ? "rgba(0, 0, 0, 0.3)" : "",
          visibility: props.button.open ? "visible" : "hidden",
          border: "1px solid rgba(255, 255, 255, 0.2)"
        }}
      >
        {props.button.method === "GET" &&
          <div className="d-flex position-relative flex-column flex-wrap mb-auto h-100">
            <div className="col-md-12 mt-5">
              <label className="px-2 pb-2" style={{ fontSize: "1rem" }} htmlFor="">Use parameter</label>
              <input
                type="checkbox"
                checked={props.button.useParam}
                onChange={() => props.changeState("useParam", props.button.id)}
              />
            </div>
            <div
              className="row mx-auto mt-5"
            >
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >id: </label>
              <input
                type="number"
                placeholder="id"
                className="me-auto"
                style={{ height: "30px", width: "60%", fontSize: "14px" }}
                value={props.button.parameters.content}
                onChange={(event) => props.changeState("parameters", props.button.id, "content", Number(event.target.value))}
              />
            </div>
            <div
              className="position-absolute m-auto"
              style={{ left: "0", right: "0", bottom: "10%" }}
            >
              <button className="btn btn-primary"
                onClick={() => props.request(props.button.id)}
              >
                CONFIRM
              </button >
            </div>
          </div>
        }
        {props.button.method === "POST" &&
          <div className="d-flex position-relative flex-column flex-wrap mb-auto h-100 w-100">
            <div className="col-md-12 mt-5">
              <label className="px-2 pb-2" style={{ fontSize: "1rem" }} htmlFor="">Use parameter</label>
              <input
                type="checkbox"
                checked={props.button.useParam}
                onChange={() => props.changeState("useParam", props.button.id)}
              />
            </div>
            <div
              className="row mx-auto mt-3"
            >
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >id: </label>
              <input
                type="number"
                placeholder="id"
                className="me-auto"
                style={{ height: "30px", width: "50%", fontSize: "14px" }}
                value={props.button.parameters.id}
                onChange={(event) => props.changeState("parameters", props.button.id, "id", Number(event.target.value))}
              />
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >content: </label>
              <input
                type="text"
                placeholder="name"
                className="me-auto"
                style={{ height: "30px", width: "50%", fontSize: "14px" }}
                value={props.button.parameters.content}
                onChange={(event) => props.changeState("parameters", props.button.id, "content", event.target.value)}
              />
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >importance: </label>
              <input
                type="checkbox"
                checked={props.button.parameters.important}
                onChange={(event) => props.changeState("parameters", props.button.id, "important", Boolean(event.target.value))}
              />
            </div>
            <div
              className="position-absolute m-auto"
              style={{ left: "0", right: "0", bottom: "10%" }}
            >
              <button className="btn btn-primary"
                onClick={() => props.request(props.button.id)}
              >
                CONFIRM
              </button >
            </div>
          </div>
        }
        {props.button.method === "PUT" &&
          <div className="d-flex position-relative flex-column flex-wrap mb-auto h-100 w-100">
            <div className="col-md-12 mt-5">
              <label className="px-2 pb-2" style={{ fontSize: "1rem" }} htmlFor="">Use parameter</label>
              <input
                type="checkbox"
                checked={props.button.useParam}
                onChange={() => props.changeState("useParam", props.button.id)}
              />
            </div>
            <div
              className="row mx-auto mt-3"
            >
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >id: </label>
              <input
                type="number"
                placeholder="id"
                className="me-auto"
                style={{ height: "30px", width: "50%", fontSize: "14px" }}
                value={props.button.parameters.id}
                onChange={(event) => props.changeState("parameters", props.button.id, "id", Number(event.target.value))}
              />
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >content: </label>
              <input
                type="text"
                placeholder="name"
                className="me-auto"
                style={{ height: "30px", width: "50%", fontSize: "14px" }}
                value={props.button.parameters.content}
                onChange={(event) => props.changeState("parameters", props.button.id, "content", event.target.value)}
              />
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >importance: </label>
              <input
                type="checkbox"
                checked={props.button.parameters.important}
                onChange={(event) => props.changeState("parameters", props.button.id, "important", Boolean(event.target.value))}
              />
            </div>
            <div
              className="position-absolute m-auto"
              style={{ left: "0", right: "0", bottom: "10%" }}
            >
              <button className="btn btn-primary"
                onClick={() => props.request(props.button.id)}
              >
                CONFIRM
              </button >
            </div>
          </div>
        }
        {props.button.method === "DELETE" &&
          <div className="d-flex position-relative flex-column flex-wrap mb-auto h-100">
            <div className="col-md-12 mt-5">
              <label className="px-2 pb-2" style={{ fontSize: "1rem" }} htmlFor="">Use parameter</label>
              <input
                type="checkbox"
                checked={props.button.useParam}
                onChange={() => props.changeState("useParam", props.button.id)}
              />
            </div>
            <div
              className="row mx-auto mt-5"
            >
              <label style={{ height: "30px", width: "30%", fontSize: "1rem", textAlign: "left", marginRight: "auto" }} >id: </label>
              <input
                type="number"
                placeholder="id"
                className="me-auto"
                style={{ height: "30px", width: "50%", fontSize: "14px" }}
                value={props.button.parameters.id}
                onChange={(event) => props.changeState("parameters", props.button.id, "id", Number(event.target.value))}
              />
            </div>
            <div
              className="position-absolute m-auto"
              style={{ left: "0", right: "0", bottom: "10%" }}
            >
              <button className="btn btn-primary"
                onClick={() => props.request(props.button.id)}
              >
                CONFIRM
              </button >
            </div>
          </div>
        }
        <div
          className="d-flex position-absolute justify-content-center align-content-center"
          style={{ top: 0, right: 0, marginTop: "-20px", marginRight: "-8px" }}
          onClick={() => props.changeState("open", props.button.id)}
        >
          <p
            style={{ textAlign: "center", margin: "auto", padding: 0, lineHeight: 0 }}
          >
            X
          </p>
        </div>
      </div>
    </div>
  )
}

const Notes = () => {

  const [buttons, setButtons] = useState<IButton[]>([
    {
      id: 0,
      method: "GET",
      open: false,
      useParam: true,
      parameters: {
        id: 0,
        content: "",
        date: "",
        important: false
      },
    },
    {
      id: 1,
      method: "POST",
      open: false,
      useParam: true,
      parameters: {
        id: 0,
        content: "",
        date: "",
        important: false
      },
    },
    {
      id: 2,
      method: "PUT",
      open: false,
      useParam: true,
      parameters: {
        id: 0,
        content: "",
        date: "",
        important: false
      },
    },
    {
      id: 3,
      method: "DELETE",
      open: false,
      useParam: true,
      parameters: {
        id: 0,
        content: "",
        date: "",
        important: false
      },
    },
  ]);



  const [notes, setNotes] = useState<INote[]>([]);

  const getNotes = async (parameter?: string) => {
    NoteService.getNote(parameter)
      .then((response) => {
        if (parameter)
          setNotes([response.data]);
        else
          setNotes(response.data);
      });
  }
  const postNotes = async (parameters: INote) => {
    const newNote: INote = {
      id: parameters.id ? parameters.id : notes.length + 1,
      content: parameters.content ? parameters.content : "Empty",
      date: new Date().toISOString(),
      important: parameters.important ? parameters.important : false
    };

    NoteService.postNote(newNote)
      .then((response) => setNotes([...notes, response.data]));
  }
  const putNotes = async (parameters: INote) => {
    const newNote: INote = {
      id: parameters.id ? parameters.id : notes.length + 1,
      content: parameters.content ? parameters.content : "Empty",
      date: new Date().toISOString(),
      important: parameters.important ? parameters.important : false
    };

    NoteService.updateNote(newNote)
      .then((response) => {
        setNotes([...notes, response.data])
        getNotes();
      });
  }
  const deleteNotes = (id: number) => {
    NoteService.deleteNote(id)
      .then((response) => {
        getNotes();
      })
  }

  const toggleImportance = async (id: number) => {
    let note: INote | undefined;
    let changedNote: INote;

    note = notes.find(n => n.id === id);

    if (note) {
      changedNote = { ...note, important: !note?.important };
      if (changedNote) {
        NoteService.updateNote(changedNote)
          .then((response) => {
            setNotes(notes.map(
              note => note.id === id ? response.data : note
            ))
          });
      }
    }
  }

  const changeState = (propertyString: string, id: number, param: string, value: string) => {
    const newState = buttons.map(button => {
      const propertyKey = propertyString as keyof IButton;
      if (button.id === id) {
        if (param) {
          return { ...button, ["parameters"]: { ...button["parameters"], [param]: value } }
        }
        else {
          return { ...button, [propertyString]: !button[propertyKey] };
        }
      }
      return button;
    })

    setButtons(newState);
    console.log(newState);
  }

  const request = (id: number) => {
    const button = buttons.find(button => button.id === id)
    if (button?.method === "GET") {
      getNotes(button.useParam ? button.parameters["content"] : undefined);
    }
    else if (button?.method === "POST") {
      postNotes(button.parameters);
    }
    else if (button?.method === "PUT") {
      putNotes(button.parameters);
    }
    else if (button?.method === "DELETE") {
      deleteNotes(button.parameters.id);
    }
  }

  useEffect(() => {
    getNotes();
  }, [])

  return (
    <div className="d-flex col-md-12 row justify-content-center align-items-center">
      <div className="col-md-12 row justify-content-center align-items-center">
        {buttons.map((button, key) => {
          return (
            <Button
              key={key}
              button={button}
              changeState={changeState}
              request={request}
            />
          );
        })}
        {notes.map((element, key) => {
          return (
            <Note key={key} note={element} toggle={toggleImportance} />
          );
        })}
      </div>
    </div>
  );
};


export default Notes;