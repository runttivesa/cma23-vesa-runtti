import axios from 'axios'
import { INote } from './data';
const baseUrl = 'http://localhost:5000/notes';

const get = async () => {
  return await axios.get(baseUrl);
}

const post = async (newObject: INote) => {
  return await axios.post(baseUrl, newObject);
}

const update = async (id: number, newObject: INote) => {
  return await axios.put(`${baseUrl}/${id}`, newObject);
}

const exportedObject = {
  get,
  post,
  update
};

export default exportedObject;