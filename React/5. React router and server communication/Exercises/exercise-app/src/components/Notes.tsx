import { useEffect, useState } from "react";
import NoteService from "./NoteService";
import { INote } from "./data";
import { Note } from "./Note";

const Notes = () => {

  
  const [notes, setNotes] = useState<INote[]>([]);


  const getNotes = async () => {
    NoteService.get()
      .then((response) => setNotes(response.data));
  }


  const postNotes = async () => {
    const newNote: INote = {
      id: notes.length + 1,
      content: "Herp Derp",
      date: new Date().toISOString(),
      important: false
    };

    NoteService.post(newNote)
      .then((response) => setNotes([...notes, response.data]));
  }


  const toggleImportance = async (id: number) => {
    let note: INote | undefined;
    let changedNote: INote;

    note = notes.find(n => n.id === id);

    if (note) {
      changedNote = { ...note, important: !note?.important };
      if (changedNote) {
        NoteService.update(id, changedNote)
          .then((response) => {
            setNotes(notes.map(
              note => note.id === id ? response.data : note
            ))
          });
      }
    }
  }


  useEffect(() => {
    getNotes();
  }, [])


  return (
    <div className="d-flex col-md-8 row justify-content-center align-items-center">

      <div className="col-md-4">
        <button
          className="btn btn-primary mx-4"
          autoCapitalize="true"
          onClick={() => getNotes()}
        >
          Get
        </button>
        <button
          className="btn btn-primary mx-4"
          autoCapitalize="true"
          onClick={() => postNotes()}
        >
          Post
        </button>
      </div>

      {notes.map((element, key) => {
        return (
          <Note key={key} note={element} toggle={toggleImportance} />
        );
      })}
    </div>
  );
};


export default Notes;