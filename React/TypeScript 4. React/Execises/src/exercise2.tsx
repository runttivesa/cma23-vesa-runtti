import React, { useState, useEffect } from "react";

/* Styles */
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';


const Emperor = (props: { setCody: React.Dispatch<React.SetStateAction<boolean>> }) => {

  const [texts, setTexts] = useState<string[]>([])
  const emperorMessages: string[] = ["Execute...", "Order...", "66... !"]

  useEffect(() => {
    setTimeout(() => {
      emperorMessages.forEach((text, i) => {
        /* Add each message from emperor to texts after timeout */
        setTimeout(() => { setTexts((prevState) => [...prevState, text]); }, 500 * i);
        if (i >= 2) {
          props.setCody(true)
        }
      });
    }, 3000);
  }, []);

  return (
    <>
      <div className="drawer-emperor">
        <div className="glitch">
          <img className="emperor" src="https://www.superherodb.com/pictures2/portraits/10/050/10462.jpg?v=1609027200" alt="" />
          <div className="glitch__layers">
            <div className="glitch__layer"></div>
            <div className="glitch__layer"></div>
            <div className="glitch__layer"></div>
          </div>
        </div>
      </div>
      <div className="d-flex row justify-content-between align-items-center" style={{ top: "rem" }}>
        <span>
          {texts.map((text) => (
            <h5 className="text-white d-inline-block" key={text}>{text}</h5>
          ))}
        </span>
      </div>
    </>
  );
};


const Cody = (props: { setOrder: React.Dispatch<React.SetStateAction<string>>, order: string }) => {

  const [name, setName] = useState<string>("")

  return (
    <div className="drawer-cody">
      <div className="glitch">
        <div className="cody">
          <img src="https://static.wikia.nocookie.net/disney/images/d/d3/Commander_Cody-0.jpg" alt="" />
        </div>
      </div>
      <span>
        <p className="text-white d-inline-block">Commander</p>
        <input className="bg-transparent mx-2 text-white w-25" value={name} type="text" onChange={(event) => { setName(event.target.value) }} />
        <p className="text-white d-inline-block">here, ready to serve !</p>
        <br />
        {name &&
          <>
            <p className="text-white d-inline-block">Executing order</p>
            <input className="bg-transparent mx-2 text-white w-25" value={props.order} type="text" onChange={(event) => { props.setOrder(event.target.value) }} />
            <p className="text-white d-inline-block">.</p>
          </>
        }
        <br />
      </span>
    </div>
  )
}


export const Exercise2 = () => {
  const [cody, setCody] = useState<boolean>(false)
  const [execute, setExecute] = useState<boolean>(false)
  const [order, setOrder] = useState<string>("")


  
  return (
    <main>
      {execute &&
        < img src="https://media.tenor.com/KsPeM0nxisEAAAAC/ion-cannon-star-wars.gif" alt="" />
      }
      {!execute &&
        <Emperor setCody={setCody} />
      }
      {cody && !execute &&
        <Cody setOrder={setOrder} order={order} />
      }
      {order === "66" &&
        <div className="position-absolute" style={{ top: "80%", margin: 0, msTransform: "translateY(-50%)", transform: "translateY(-50%)" }}>
          <button className="btn" style={{ color: "white", border: "2px solid" }} onClick={() => setExecute(!execute)}>{execute ? "STOP" : "EXECUTE"}</button>
        </div>
      }
    </main>
  )
}