import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, Link } from "react-router-dom";
import { Exercise1 } from "./exercise1";
import { Exercise2 } from "./exercise2";

const router = createBrowserRouter([
    {
        path: "/",
        element: (
            <div>
                <br />
                <Link to="/exercise1">Exercise 1</Link>
                <br />
                <br />
                <Link to="/exercise2">Exercise 2</Link>
            </div>
        ),
    },
    {
        path: "/exercise1",
        element: <Exercise1 />,
    },
    {
        path: "/exercise2",
        element: <Exercise2 />,
    },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
    <RouterProvider router={router} />
);